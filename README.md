# Simple Lua library for computations involving graphs etc.

Documentation can be generated with ldoc or doxygen.

See [here](https://tcslib.gitlab.io/tcsliblua/) for the
pre-generated ldoc documentation.

