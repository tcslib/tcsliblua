--- k-dim Weisfeiler-Lehman algorithm
--- @module wl
local oo = require "oo"
local makeclass = oo.makeclass
require "graphs"

local M = {
  MODINFO = {
    name = "wl",
    links = {}
  }
}
--- Replacement function for the case, where V={0,..,n-1} and k-tuples
--- are encoded as n-ary numbers, and kWL will run for one graph.
--- A replacement function take an encoded tuple t=(u1,...,ui,...),
--- a position i and the element v to put in place i. Then it return the
--- encoding for the tuple t'=(u1,...,ui-1,v,ui+1,...).
M.replaceInt = function(n)
  return function(t,i,v)
    local ti = t % (n^i)
    ti = ti - (ti % (n^(i-1)))
    return t - ti + v*n^(i-1)
  end
end
local replaceInt = M.replaceInt

--- Similar to replaceInt, but for 2 graphs.
--- Here not all (2n)^k tuples are considered, but
--- only the 2(n^k) where all elements come from the same graph, thus
--- replacement depends on the graph the tuple is taken from.
M.replaceInt2 = function(n,k)
  rin = replaceInt(n)
  return function(t,i,v)
    if t >= n^k then
      return rin(t-n^k,i,v)+n^k
    else
      return rin(t,i,v)
    end
  end
end
local replaceInt2 = M.replaceInt2

--- If k-tuples are just tables of length k and __index etc. is used
--- see indexByTuple.
M.replacePlain = function(t,i,v)
  local tnew = {}
  for j=1,#t do
    tnew[j]=t[j]
  end
  tnew[i] = v
  return tnew
end
local replacePlain = M.replacePlain

--- Replacement function (like replaceInt) for k=1.
--- for 1-dim-WL, we replace the ith vertex (i is always 1) u in the
--- "tuple" t=(u) by v iff v is a neighbor of u in G, otherwise we
--- return nil/. Thus kWLsift will return "()" for non-neighbors
--- and "("..c..")" for neighbors, where c is the old color of u.
--- As this depends on the adjacency of a graph G, this function
--- returns the actual replacement function.
M.replace1d = function(am)
  return function(t,i,v)
    local u = t--t is just a vertex
    if am[u][v] then
      return v
    else
      return nil
    end
  end
end
local replace1d = M.replace1d


M.graphFromTupleInt = function(n,k)
  local nk = n^k
  return function(t)
    return (t // nk)+1
  end
end
local graphFromTupleInt = M.graphFromTupleInt

--- We plan to use color[t] for, e.g., t={3,8,7} in 3-WL
--- Internally color[t] is stored at
--- color.indexByTuple[3][8][7]
--- Note that kWLMain never assigns a new table to color.
M.indexByTuple = {
  __index = function(t,i)
    if type(i) ~= "table" then
      return rawget(t,i)
    end
    local val = rawget(t,".indexByTuple")
    for _,j in ipairs(i) do
      val = val[j]
    end
    return val
  end,

  __newindex = function(t,i,val)
    if type(i) ~= "table" then
      return rawset(t,i,val)
    end
    local parent = rawget(t,".indexByTuple")
    if not parent then
      parent = {}
      rawset(t,".indexByTuple",parent)
    end
    local ch
    for l=1,#i-1 do
      local j=i[l]
      ch = parent[j]
      if not ch then
        ch = {}
        parent[j] = ch
      end
      parent = ch
    end
    parent[i[#i]]=val
  end
}
local indexByTuple = M.indexByTuple

M.kWLsift = function(k,t,replace,v,lastcol)
  local sift = {}
  for i=1,k do
    table.insert(sift,lastcol[replace(t,i,v)])
  end
  return "(" .. table.concat(sift,",") .. ")"
end
local kWLsift = M.kWLsift

local colorDict
M.colorDict = {
  new = function(init)
    init = init or {}
    return setmetatable(init,colorDict)
  end,

  ---
  --- @return boolean d1 stands before d2 or is equiv. to d2
  --- @return number r: d1,d2 can be separated in round r
  __lt = function(d1,d2)
    for r=0,math.max(#d1,#d2) do
      --check that #rounds until stable match
      local d1r,d2r = d1[r],d2[r]
      if not d1r then
        return true, r
      end
      if not d2r then
        return false, r
      end

      --check that #colors match for each round
      if #d1r < #d2r then
        return true, r
      elseif #d1r > #d2r then
        return false, r
      end
      for i=0,#d1r do
        local d1ri,d2ri = d1r[i],d2r[i]
        --compare colors
        if d1ri.c < d2ri.c then
          return true, r
        elseif d1ri.c > d2ri.c then
          return false, r
        end
        --compare their multiplicities
        if d1ri.m < d2ri.m then
          return true, r
        elseif d1ri.m > d2ri.m then
          return false, r
        end
      end
    end
    --second true for equality (s.b.)
    return true, nil
  end,

  __eq = function(d1,d2)
    --no round r to distinguish them
    return not select(2,colorDict.__lt(d1,d2))
  end,

  rdist = function(d1,d2)
    return select(2,colorDict.__lt(d1,d2))
  end,

  __tostring = function(d)
    local roundsStr = {}
    for r=0,#d do
      local rstr = {"r="..r..": "..d[r][0].c.."["..d[r][0].m .."]"}
      for i,col in ipairs(d[r]) do
        rstr[i+1] = col.c.."["..col.m .."]"
      end
      roundsStr[r+1] = table.concat(rstr,"   ")
    end
    return table.concat(roundsStr,"\n")
  end
}
colorDict = M.colorDict
oo.makeclass(colorDict)

M.kWLmain = function(vertices,tuples,replace,color,k,graphFromTuple,doprint)
  graphFromTuple = graphFromTuple or function() return 1 end
  local info = ((doprint or M.print) and print) or (function() end)
  local k = k or #tuples[1]
  local nc,nclast = 0,0 --current and last iteration's number of colors

  --save big colors:
  local cd0 = {}
  local cd = colorDict({[0]=cd0})

  local colors = {}
  for _,t in pairs(tuples) do
    table.insert(colors,color[t])
  end
  table.sort(colors)
  local invColors = {}
  for _,col in ipairs(colors) do
    if not invColors[col] then
      invColors[col] = nc
      cd0[nc] = {c=col,m=1}
      nc = nc + 1
    else
      cd0[nc-1].m = cd0[nc-1].m+1
    end
  end
  info("# of colors initially:     ",nc)

  for _,t in pairs(tuples) do
    color[t] = invColors[color[t]]
  end

  local r=0
  while nc ~= nclast do
    local rdist = nil
    r=r+1
    local lastcol = color
    color = {}
    --copy the metatable (this copies __index and __newindex)
    setmetatable(color,getmetatable(lastcol))
    local colors = {}
    local colorsByGraph = {}
    for _,t in pairs(tuples) do
      local gr = graphFromTuple(t)
      color[t] = {}
      for _,v in pairs(vertices) do
        table.insert(color[t],kWLsift(k,t,replace,v,lastcol))
      end
      table.sort(color[t])
      color[t] =  lastcol[t] .. ',' .. table.concat(color[t],",")
      table.insert(colors,color[t])
      colorsByGraph[gr] = colorsByGraph[gr] or {}
      table.insert(colorsByGraph[gr],color[t])
    end

    table.sort(colorsByGraph[1])
    local grOneColor = table.concat(colorsByGraph[1],";;")
    for gr,col in ipairs(colorsByGraph) do
      table.sort(colorsByGraph[gr])
      local grCol = table.concat(colorsByGraph[gr],";;")
      if grCol ~= grOneColor then
        rdist = r
        info("Graph "..gr.." distinguished from first graph in round "..r)
      end
    end

    table.sort(colors)
    local cdr = {}
    cd[r] = cdr
    nclast=nc
    nc = 0
    local invColors = {}
    for _,col in ipairs(colors) do
      if not invColors[col] then
        invColors[col] = nc
        --multiplicity is initally one in color dict.
        cdr[nc] = {c=col,m=1}
        nc = nc + 1
      else
        --increase multiplicity in color dict.
        cdr[nc-1].m = cdr[nc-1].m+1
      end
    end
    info("# of colors after round " ..r..":",nc)
    --small colors (big colors are saved by cd)
    for _,t in pairs(tuples) do
      color[t] = invColors[color[t]]
    end
  end
  info("Stable after "..(r-1).." round(s).")
  info("=============================")
  return color,nc,r,cd,rdist
end
local kWLmain = M.kWLmain

M.onecolor = function(a,b)
  return 0
end
local onecolor = M.onecolor

M.graphTo1WL = function(method,G,vcolor,ecolor)
  vcolor = vcolor or onecolor
  ecolor = ecolor or onecolor
  local vertices, intToVert,tuples,color = {},{},{},{}
  local n = (type(G.V) == "number" and G.V) or #G.V
  local i=0
  for v in G:vertices() do
    vertices[i]=i
    color[i]=vcolor(v)
    intToVert[i]=v
    i = i+1
  end
  local am = {}
  for u=0,n-1 do
    am[u] = {}
    for v=0,n-1 do
      am[u][v] = G:hasEdge(intToVert[u],intToVert[v])
    end
  end
  return vertices,vertices,replace1d(am),color,intToVert
end
local graphTo1WL = M.graphTo1WL

M.twoGraphsTo1WL = function(method,G,H,vcolorG,vcolorH,ecolorG,ecolorH)
  vcolorG = vcolorG or onecolor
  vcolorH = vcolorH or onecolor
  ecolorG = ecolorG or onecolor
  ecolorH = ecolorH or onecolor
  local vertices, intToVert,tuples,color = {},{},{},{}
  local n = (type(G.V) == "number" and G.V) or #G.V
  assert(n==
    ((type(H.V) == "number" and H.V) or #H.V),
    "The graphs do not have the same number of vertices."
  )
  local i=0
  for v in G:vertices() do
    vertices[i]=i
    color[i]=vcolorG(v)
    intToVert[i]=v
    i = i+1
  end
  for v in H:vertices() do
    vertices[i]=i
    color[i]=vcolorH(v)
    intToVert[i]=v
    i = i+1
  end
  local am = {}
  for u=0,n-1 do
    am[u] = {}
    for v=0,n-1 do
      am[u][v] = G:hasEdge(intToVert[u],intToVert[v])
    end
  end
  for u=n,2*n-1 do
    am[u] = {}
    for v=n,2*n-1 do
      am[u][v] = H:hasEdge(intToVert[u],intToVert[v])
    end
  end
  return vertices,vertices,replace1d(am),color,intToVert
end
local twoGraphsTo1WL = M.twoGraphsTo1WL


M.graphTo2WL = function(method,G,vcolor,ecolor)
  vcolor = vcolor or onecolor
  ecolor = ecolor or onecolor
  if method == "tuplesAsIntegers" then
    local vertices, intToVert,tuples,color = {},{},{},{}
    local n = (type(G.V) == "number" and G.V) or #G.V
    local i=0
    for v in G:vertices() do
      vertices[i]=i
      intToVert[i]=v
      i = i+1
    end
    i=0
    for u=0,n-1 do
      for v=0,n-1 do
        tuples[i] = i
        color[i] =
          ((G:hasEdge(intToVert[u],intToVert[v]) and "1")  or
          (u==v and "0") or "2") ..
          "(" .. vcolor(intToVert[u]) .. "," ..
          vcolor(intToVert[v]) .. ")" ..
          ecolor(intToVert[u],intToVert[v])
        i=i+1
      end
    end
    return vertices,tuples,replaceInt(n),color,intToVert
  elseif method == "plainTuples" then
    local vertices,tuples,color = {},{},{}
    setmetatable(color,indexByTuple)
    local i=0
    for v in G:vertices() do
      vertices[i]=v
      i = i+1
    end
    i=0
    for u in G:vertices() do
      for v in G:vertices() do
        tuples[i] = {u,v}
        color[{u,v}] =
          ((G:hasEdge(u,v) and "1")  or
          (u==v and "0") or "2") ..
          "(" .. vcolor(u) .. "," ..
          vcolor(v) .. ")" ..
          ecolor(u,v)
        i=i+1
      end
    end
    return vertices,tuples,replacePlain,color
  end
end
local graphTo2WL = M.graphTo2WL

M.twoGraphsTo2WL = function(method,G,H,vcolorG,vcolorH,ecolorG,ecolorH)
  vcolorG = vcolorG or onecolor
  vcolorH = vcolorH or onecolor
  ecolorG = ecolorG or onecolor
  ecolorH = ecolorH or onecolor
  if method == "tuplesAsIntegers" then
    local vertices, intToVertG, intToVertH,tuples,color = {},{},{},{},{}
    local n = ((type(G.V) == "number") and G.V) or #G.V
    local i=0
    for v in G:vertices() do
      vertices[i]=i
      intToVertG[i]=v
      i = i+1
    end
    i=0
    for v in H:vertices() do
      intToVertH[i]=v
      i = i+1
    end
    i=0
    for u=0,n-1 do
      for v=0,n-1 do
        tuples[i] = i
        color[i] =
          ((G:hasEdge(intToVertG[u],intToVertG[v]) and "1") or
          (u==v and "0") or "2") ..
          "(" .. vcolorG(intToVertG[u]) .. "," ..
          vcolorG(intToVertG[v]) .. ")" ..
          ecolorG(intToVertG[u],intToVertG[v])
        tuples[n^2+i] = n^2+i
        color[n^2+i] =
          ((H:hasEdge(intToVertH[u],intToVertH[v]) and "1") or
          (u==v and "0") or "2") ..
          "(" .. vcolorH(intToVertH[u]) .. "," ..
          vcolorH(intToVertH[v]) .. ")" ..
          ecolorH(intToVertH[u],intToVertH[v])
        i=i+1
      end
    end
    return vertices,tuples,replaceInt2(n,2),color,intToVertG,intToVertH
  end
end
local twoGraphsTo2WL = M.twoGraphsTo2WL

M.g2d1WL = function(G,H,vcolorG,vcolorH,ecolorG,ecolorH,itv)
  local v,t,rep,c,itvG,itvH = twoGraphsTo1WL(nil,G,H,
    vcolorG,vcolorH,ecolorG,ecolorH)
  if itv then
    itv.G = itvG
    itv.H = itvH
  end
  return kWLmain(v,t,rep,c,1,graphFromTupleInt((#v+1)/2,1))
end

M.g1d1WL = function(G,vcolor,ecolor,itv)
  local v,t,rep,c,itvG = graphTo1WL(nil,G,vcolor,ecolor)
    if itv then
    itv.G = itvG
  end
  return kWLmain(v,t,rep,c,1)
end

M.g2d2WL = function(G,H,vcolorG,vcolorH,ecolorG,ecolorH,itv)
  local v,t,rep,c,itvG,itvH = twoGraphsTo2WL("tuplesAsIntegers",G,H,
    vcolorG,vcolorH,ecolorG,ecolorH)
  if itv then
    itv.G = itvG
    itv.H = itvH
  end
  return kWLmain(v,t,rep,c,2,graphFromTupleInt(#v+1,2))
end

M.g1d2WL = function(G,vcolor,ecolor,itv)
  local v,t,rep,c,itvG = graphTo2WL("tuplesAsIntegers",G,vcolor,ecolor)
  if itv then
    itv.G = itvG
  end
  return kWLmain(v,t,rep,c,2)
end

M.g1d2WLPlain = function(G,vcolor,ecolor)
  local v,t,rep,c = graphTo2WL("plainTuples",G,vcolor,ecolor)
  return kWLmain(v,t,rep,c,2)
end

return M






