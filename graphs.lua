--- graphs (as known form graph theory)
--- @defmodule graphs

local oo = require "oo"
local dcopy,perm,makeclass,chooseInd,applyPerm,seq,ikeys,gluetables,kperm,div =
  oo.dcopy,oo.perm,oo.makeclass,oo.chooseInd,oo.applyPerm,oo.seq,oo.ikeys,oo.gluetables,oo.kperm,oo.div
local Matrix = (require "matrix").Matrix
local BigInt = (require "bigint").BigInt

local canonOrderIR, colorfunc, individualize =
    oo.expectlink("graphs","indref",3)

local M = {
  MODINFO = {
    name = "graphs",
    links = {
      indref = function(indref)
	canonOrderIR = indref.canonOrderIR
	colorfunc = indref.colorfunc
	individualize = indref.individualize
      end
    }
  }
}

local valToKeys = function(t)
  local ret = {}
  for _,v in pairs(t) do
    ret[v] = true
  end
  return ret
end
--   _E = function(G,ind)
--     return G.al[ind] or G.am[ind] or false
--   end,

--- helper "class" for degree sequence walks
--- @class DSW
local DSW = {
  __tostring = function(dsw)
    local s = "seq\t#walks\tstart from(#walks)"
    for _,itm in ipairs(dsw) do
      local ds,n,verts = itm[1],itm[2],itm[3]
      s = s .. "\n("..ds[1]
      for i=2,#ds do
        s = s .. "," .. ds[i]
      end
      s = s .. ")\t" .. n .. "\t"
      s = s .. verts[1][1].."("..verts[1][2]..")"
      for i=2,#verts do
        s = s ..", ".. verts[i][1].."("..verts[i][2]..")"
      end
    end
    return s
  end
}

--- helper "class" for degree sequences
--- @class DS
local DS = {
  __tostring = function(ds)
    return "("..table.concat(ds,",")..")"
  end
}

--- helper "class" for level sequences
--- @class LS
local LS = {
  __tostring = function(ls)
    return "["..table.concat(ls,",").."]"
  end
}


local Graph
--- Graphs from graph theory.
--- Graphs have edges and vertices and may have pebbles.
--- @class Graph
--- @field V any[] vertex set
--- @field E any[] edge set
--- @field P any[] pebble tuple
--- @field Pinv any[] mapping vertex to (first) pebble
--- @field al table<any,any[]> adjacency lists for each vertex
--- @field am table<any,any[]> adjacency matrix as boolean
M.Graph = {

  --- the constructor.
  --- constructs the Graph
  --- @param V any[] the vertex set (required)
  --- for graphs with computed edges this is a number.
  --- @param E any[] the edge set (required).
  --- @param nocopy boolean default: false; if true V,E and P are **not**
  --- copied.
  --- @param P any[] a tuple of pebbles (optional) Pebbles are vertices
  --- that must be mapped onto each other (in order) by any
  --- isomorphism (a single pebble can be considered a root).
  new = function(V,E,nocopy,P)
    P = P or {}
    local G
    if getmetatable(V) == Graph then
      error("copy constructor not implemented for arbitrary graphs")
    elseif type(V) == "number" then--subclass with computed edges
      G={V=V,E=E,P=P}
    else
      G = (nocopy and {V=V,E=E,P=P}) or {V=dcopy(V),E=dcopy(E),P=dcopy(P)}
      G.al = {}
      G.am = {}
      for _,v in pairs(V) do
        G.al[v] = {}
        G.am[v] = {}
      end
      for _,e in pairs(E) do
        local u,v = table.unpack(e)
        table.insert(G.al[u],v)
        table.insert(G.al[v],u)
        G.am[v][u] = true
        G.am[u][v] = true
      end
      G.Pinv = {}
      G.npv = 0
      for i=1,#G.P do
        if not G.Pinv[G.P[i]] then
          G.Pinv[G.P[i]] = {}
          G.npv = G.npv + 1
        end
        G.Pinv[G.P[i]][#(G.Pinv[G.P[i]])+1] = i
      end
    end
    setmetatable(G,Graph)
    return G
  end,

  --- creates a Graph from an adjacency matrix given as a string.
  --- @param s string String containing the matrix.
  --- @return Graph 'created graph'
  fromAdjacencyMatrixString = function(s)
    local V,E = {},{}
    local u = 0
    local n = nil
    for line in s:gmatch("[^\n]+") do
      u = u+1
      V[u] = u
      local v = 0
      for e in line:gmatch("%D*([01])") do
        v = v + 1
        if e == "1" and v > u then
          table.insert(E,{u,v})
        end
      end
      n = n or v
      if n ~= v then
        return nil
      end
    end
    if u ~= n then
      return nil
    end
    --likely a single comment line that contains 1 or 0:
    if n==1 and s:find("%a") then
      return nil
    end
    return Graph(V,E,false)
  end,

  --- creates a list of  Graphs from multiple adjacency matrix
  --- given as a string.
  --- @param s string String containing the matrices separated by \n\n.
  --- @return Graph[] 'list of graphs'
  fromAdjacencyMatrixStringMulti = function(s)
    s = s.."\n\n"
    local list = {}
    for ms in string.gmatch(s,"(.-)\n\n") do
      table.insert(list,Graph.fromAdjacencyMatrixString(ms))
    end
    return list
  end,

  --- number of vertices.
  --- @param G Graph
  --- @return integer 'number of vertices'.
  n = function(G)
    return (type(G.V) == "number" and G.V) or #G.V
  end,

  --- number of edges.
  --- @param G Graph
  --- @return integer 'number of edges'.
  m = function(G)
    return (type(G.E) == "number" and G.E) or #G.E
  end,

  --- a descriotiuon used to create the graph.
  --- For subclasses such as Tree the degree or level sequence
  --- is considered its specification. Otherwise just a verbal
  --- description wit vertices and edges.
  --- @param G Graph
  --- @return string 'specification string'
  spec = function(G)
    return G.specstr or
      ("Graph with ".. G:n() .." vertices and "..G:m().." edges")
  end,

  --- converts the vertices to integers.
  --- G:vertex(i) becomes i, edges {G:vertex(i), G:vertex(j)}
  --- become {i,j}.
  --- @param G Graph
  --- @return Graph 'where V and E are actual sets of integers'.
  toIntGraph = function(G)
    local V,E,P = {}, {}, {}
    local vindex = {}
    for i=1,G:n() do
      V[i] = i
      vindex[G:vertex(i)] = i
    end
    for i=1,#G.P do
      P[i] = vindex[G.P[i]]
    end
    local e = 0
    for i=1,G:n() do
      for j=i+1,G:n() do
        if G:hasEdge(G:vertex(i),G:vertex(j)) then
          e = e+1
          E[e] = {i,j}
        end
      end
    end
    return Graph(V,E,false,P)
  end,

  --- adds an edge to the graph.
  --- @param G Graph
  --- @param u any vertex of G or entire edge
  --- @param v any|nil vertex of G or nil
  addEdge = function(G,u,v)
    local e = u
    if v then
      e = {u,v}
    end
    table.insert(G.E,e)
    u,v = table.unpack(e)
    table.insert(G.al[u],v)
    table.insert(G.al[v],u)
    G.am[v][u] = true
    G.am[u][v] = true
  end,

  --- removes an edge to the graph.
  --- @param G Graph
  --- @param u any vertex of G or entire edge
  --- @param v any|nil vertex of G or nil
  removeEdge = function(G,u,v)
    if not G:hasEdge(u,v) then
      return
    end
    local e = u
    if v then
      e = {u,v}
    end
    u,v = table.unpack(e)
    local ei = 1
    while not (
        (G.E[ei][1] == u and G.E[ei][2] == v) or
        (G.E[ei][1] == v and G.E[ei][2] == u)) do
      ei = ei+1
    end
    table.remove(G.E,ei)

    local ui=1
    while(G.al[v][ui] ~= u) do ui = ui+1 end
    table.remove(G.al[v],ui)
    local vi=1
    while(G.al[u][vi] ~= v) do vi = vi+1 end
    table.remove(G.al[u],vi)

    G.am[v][u] = false
    G.am[u][v] = false
  end,

  --- appends a peppble to the graph's list of pebbles.
  --- @param G Graph
  --- @param peb any[]
  addPebbles = function(G,peb)
    local np = #G.P
    for _,v in pairs(peb) do
      np=np+1
      G.P[np] = v
      if not G.Pinv[v] then
        G.Pinv[v] = {}
        G.npv = G.npv + 1
      end
      G.Pinv[v][#(G.Pinv[v])+1] = np
    end
  end,

  --- returns the ith vertex.
  --- for non-computed graphs this is G.V[i]
  --- @param G Graph
  --- @param i integer
  --- @return any
  vertex = function(G,i)
    if G._vertex then
      return G._vertex(i)
    end
    return G.V[i]
  end,

  --- returns the ith edge.
  --- for non-computed graphs this is G.E[i]
  --- @param G Graph
  --- @param i integer
  --- @return any[]|nil
  edge = function(G,i)
    if i > G:m() then
      return nil
    end
    if G._edge then
      return G._edge(i)
    end
    return G.E[i]
  end,

  --- returns the ith pebble.
  --- for non-computed graphs this is G.P[i]
  --- @param G Graph
  --- @param i integer
  --- @return any
  pebble = function(G,i)
    if G._pebble then
      return G._pebble(i)
    end
    return G.P[i]
  end,

  --- returns an iterator over all vertices.
  --- @param G Graph
  --- @return function
  vertices = function(G)
    if G._vertices then
      return G._vertices()
    end
    local i = 0
    return function()
      i = i + 1
      return G:vertex(i)
    end
  end,

  --- returns an iterator over all edge.
  --- @param G Graph
  --- @return function
  edges = function(G)
    if G._edges then
      return G._edges()
    end
    local i = 0
    return function()
      i = i + 1
      return G:edge(i)
    end
  end,

  --- returns an iterator over all pebbles.
  --- @param G Graph
  --- @return function
  pebbles = function(G)
    if G._pebbles then
      return G._pebbles()
    end
    local i = 0
    return function()
      i = i + 1
      return G:pebble(i)
    end
  end,

  --- checks if {u,v} is an edge of the graph.
  --- @param G Graph
  --- @param u any vertex of G or entire edge
  --- @param v any|nil vertex of G or nil
  --- @return boolean
  hasEdge = function(G,u,v)
    if not v then
      v=u[2]
      u=u[1]
    end
    if G._hasEdge then
      return G._hasEdge(u,v)
    end
    return G.am[u] and G.am[u][v] or false
  end,

  --- returns the index of the pebble on v or false if there is none.
  --- @param G Graph
  --- @param v any vertex of G
  --- @return integer|boolean
  pebblesOn = function(G,v)
    if G._pebblesOn then
      return G._pebblesOn(v)
    end
    return G.Pinv[v] or false
  end,

  --- returns a table of degrees indexed by vertices
  --- @param G Graph
  --- @return integer[]
  degrees = function(G)
    if G._degrees then
      return dcopy(G._degrees)
    end
    local deg = {}
    for e in G:edges() do
      deg[e[1]] = (deg[e[1]] or 0) + 1
      deg[e[2]] = (deg[e[2]] or 0) + 1
    end
    return deg
  end,

  --- partitions vertices by degree.
  --- @param G Graph
  --- @return any[][]
  degPart = function(G)
    local part = {}
    for v,d in pairs(G:degrees()) do
      part[d] = part[d] or {}
      table.insert(part[d],v)
    end
    return part
  end,

  --- returns an iterator over all neighbors of v.
  --- @param G Graph
  --- @param v any vertex of G
  --- @return function
  neighbors = function(G,v)
    if G._neighbors then
      return G._neighbors(v)
    end
    local i = 0
    return function()
      i = i + 1
      if i <= #G.al[v] then
        return G.al[v][i]
      else
        return nil
      end
    end
  end,

  --- returns the neighborhood as a table.
  --- @param G Graph
  --- @param v any vertex of G
  --- @return function
  neighborhood = function(G,v)
    if G._neighbors then
      local N  = {}
      for u in G._neighbors(v) do
        table.insert(N,u)
      end
      return N
    end
    return dcopy(G.al[v],1)
  end,

  --- checks if N(v)\{u} = N(u)\{v}, where N is the neighborhood.
  --- @param G Graph
  --- @param u any vertex of G
  --- @param v any vertex of G
  --- @return boolean
  areTwins = function(G,u,v)
    local nbv = {}
    for w in G:neighbors(v) do
      nbv[w] = true
    end
    for w in G:neighbors(u) do
      if not nbv[w] then
        return false
      else
        nbv[w] = nil
      end
    end--N(u)\{u} subset of N(v)
    --check that deg(v) is not larger:
    return not next(nbv)
  end,

  --- returns the graph with edge set {{u,v}\subseteq V| {u,v}\notin E}.
  --- pebbles keep their position,
  --- @param G Graph
  --- @return Graph
  complement = function(G)
    if G._complement then
      return G._complement()
    end
    local Vc,Ec = {},{}
    for u in G:vertices() do
      for _,v in ipairs(Vc) do
        if not G:hasEdge(u,v) then
          table.insert(Ec,{u,v})
        end
      end
      table.insert(Vc,u)
    end
    return Graph(Vc,Ec,false,G.P)
  end,

  --- Disjoint union of graphs.
  --- Ignores pebbles.
  --- @param G Graph
  --- @param H Graph
  --- @return Graph
  disjointUnion = function(G,H)
    G = G:toIntGraph()
    H = H:toIntGraph()
    local Vu,Eu = G.V,G.E
    local n = G:n()
    for _,u in ipairs(H.V) do
      table.insert(Vu,u+n)
    end
    for _,e in ipairs(H.E) do
      local u,v = e[1],e[2]
      table.insert(Eu,{u+n,v+n})
    end
    return Graph(Vu,Eu,false)
  end,

  --- computes a graph G' with integer vertices first and
  --- then permutes edges and pebbles accoirding to pi.
  --- {u,v} -> {pi(u),pi(v)}.
  --- @param G Graph
  --- @param pi integer[] assumed to be a permutation on {1,...,G:n()}
  --- @return Graph
  permuted = function(G,pi)
    G = G:toIntGraph()
    for _,e in pairs(G.E) do
      e[1] = pi[e[1]]
      e[2] = pi[e[2]]
    end
    for i=1,#G.P do
      G.P[i] = pi[G.P[i]]
    end
    return Graph(G.V,G.E,"nocopy",G.P)
  end,

  --- checks if G is isomorphic to H
  --- this currently **does not respect pebbles**
  --- but will likely do so **in the future**.
  --- uses an naive implementation
  --- (no intelligent backtracking) of
  --- individualization-refinement and thus maybe costly
  --- @param G Graph
  --- @param H Graph
  --- @return boolean
  isIsomorphicTo = function(G,H)
    local ordG,ordH = canonOrderIR(G), canonOrderIR(H)
    return G:adjString(ordG) == H:adjString(ordH)
  end,

  --- computes the orbits accoding to Graph.isIsomorphicTo().
  --- @see Graph.isIsomorphicTo for problems.
  --- The colors in vcolor are respected, the order in vorder is used to
  --- sort the orbits: If vorder corresponds to u,v,x,y,z
  --- and orbit are {x,v} and {u,y,z} then they will be returned as
  --- [[u,y,z],[x,v]]. If no order is given the canonical order
  --- from canonOrderIR(G) is used.
  --- @param G Graph
  --- @param vcolor function|table?
  --- @param vorder integer[]?
  --- @return Graph
  orbits = function(G,vcolor,vorder)
    local ord = vorder or canonOrderIR(G)
    local orbits = {}
    local canadjseen = {}
    for _,v in ipairs(ord) do
      local vicolor = individualize(vcolor,G:vertex(v))
      local vord = canonOrderIR(G,vicolor)
      local vadj = G:colorString(vicolor,vord)..G:adjString(vord)
      local orbn = canadjseen[vadj]
      if not orbn then
        orbn = #orbits + 1
        orbits[orbn] = {v}
        canadjseen[vadj] = orbn
      else
        table.insert(orbits[orbn],v)
      end
    end
    return orbits
  end,

  --- returns table of represantatives of
  --- the factor group (Sym(G.V)/Aut(G))
  --- @param G Graph
  --- @return integer[][]
  automFactGrp = function(G)
    if G.autFG then
      return G.autFG
    end
    local Gint = G:toIntGraph()
    local amFound,fctGrp = {},{}
    for pi in perm(Gint.V) do
      local H = Gint:permuted(pi)
      if not amFound[H:adjString()] then
        amFound[H:adjString()] = true
        table.insert(fctGrp,pi)
      end
    end
    G.autFG = fctGrp
    return fctGrp
  end,

  --- counts the number of subgraphs of G
  --- that are isomorphic to F **devided by
  --- the size of Aut(F)**
  --- this is faster than countSubIso
  --- @param G Graph host graph
  --- @param F Graph pattern graph
  --- @return integer
  countSub = function(G,F)
    F = F:toIntGraph()
    G = G:toIntGraph()--seems faster
    local occ = 0
    local fctGrp = F:automFactGrp()
    for s in chooseInd(G:n(),F:n()) do
      for _,pi in pairs(fctGrp) do
        local c = applyPerm(s,pi)
        local found = 1
        for e in F:edges() do
          if not G:hasEdge(c[e[1]],c[e[2]]) then
            found = 0
            break
          end
        end
        occ = occ + found
      end
    end
    return occ
  end,

  --- counts the number of subgraphs of G
  --- that are isomorphic to F
  --- @param G Graph host graph
  --- @param F Graph pattern graph
  --- @return integer
  countSubIso = function(G,F)
    if F:pebble(1) then
      return G:countSubIsoPeb(F)
    end
    F = F:toIntGraph()
    G = G:toIntGraph()--seems faster
    local Vexp = {}
    for i=1,G:n() do
      Vexp[i] = G:vertex(i)
    end
    local occ = 0
    for c in kperm(Vexp,F:n()) do
      local found = 1
      for e in F:edges() do
        if not G:hasEdge(c[e[1]],c[e[2]]) then
          found = 0
          break
        end
      end
      occ = occ + found
    end
    return occ
  end,

  --- returns a mapping between the pebbles of
  --- F and G and its inverse.
  --- @param G Graph
  --- @param F Graph
  --- @return table|nil
  --- @return table|nil
  pebbleInjRestr = function(G,F)
    if #G.P ~= #F.P then
      return nil
    end
    local inj = {}
    local inv = {}
    for i=1,#G.P do
      if inj[F.P[i]] then
        if inj[F.P[i]] ~= G.P[i] then
          return nil
        end
      elseif inv[G.P[i]] then
        if inv[G.P[i]] ~= F.P[i] then
          return nil
        end
      else
        inj[F.P[i]] = G.P[i]
        inv[G.P[i]] = F.P[i]
      end
    end
    return inj, inv
  end,

  --- permutes the vertex set of a graph with
  --- integer vertices such that the c pebbles sit on
  --- the vertice n-c+1,...,n.
  --- @param G Graph intGraph
  --- @return Graph
  pebbleLast = function(G)
    local pi = {}
    local lastav = G:n()
    local fixfrom = G:n()-G.npv+1--pebbles already at the end stay
    for i=1,G:n() do
      if (G.Pinv[i] and i >= fixfrom) or
          ((not G.Pinv[i]) and i < fixfrom) then
        pi[i] = i
      elseif G.Pinv[i] then
        while G.Pinv[lastav] do
          lastav = lastav - 1
        end
        pi[i] = lastav
        pi[lastav] = i
        lastav = lastav - 1
      end
    end
    return G:permuted(pi)
  end,

  --- counts the number of subgraphs of G
  --- that are isomorphic to F if both
  --- F and G are pebbled. This method is automatically called
  --- by Graph.countSubIso as needed.
  --- @param G Graph host graph
  --- @param F Graph pattern graph
  --- @return integer
  countSubIsoPeb = function(G,F)
    F = F:pebbleLast()--implies toIntGraph()
    G = G:toIntGraph()--seems faster
    local inj = G:pebbleInjRestr(F)
    local Vexp,nv = {},0
    for i=1,G:n() do
      if not G:pebblesOn(G:vertex(i)) then
        nv=nv+1
        Vexp[nv] = G:vertex(i)
      end
    end
    local occ = 0
    for c in kperm(Vexp,F:n()-F.npv) do
      for k,v in pairs(inj) do
        c[k] = v
      end
      local found = 1
      for e in F:edges() do
        if not G:hasEdge(c[e[1]],c[e[2]]) then
          found = 0
          break
        end
      end
      occ = occ + found
    end
    return occ
  end,

  --- computes a string representation of the adjacency matrix
  --- where columns and rows are sorted according to vorder.
  --- @param G Graph
  --- @param vorder integer[]
  --- @return string
  adjString = function(G,vorder)
    vorder = vorder or seq(1,G:n())
    local adj, line = {}, nil
    for i,vi in ipairs(vorder) do
      line = {}
      for j,vj in ipairs(vorder) do
        line[j] = (G:hasEdge(G:vertex(vi),G:vertex(vj)) and 1) or 0
      end
      adj[i] = table.concat(line) .. "\n"
    end
    return table.concat(adj)
  end,

  --- computes a string representing the vertex colors
  --- in order vorder.
  --- Together with canAdjString it forms a complete
  --- invariant for colored graphs.
  --- @param G Graph
  --- @param vcolor table|function
  --- @param vorder integer[]
  --- @return string
  colorString = function(G,vcolor,vorder)
    vorder = vorder or seq(1,G:n())
    vcolor = colorfunc(vcolor)
    local ret = {}
    for i,v in ipairs(vorder) do
      ret[i] = vcolor(G:vertex(v))
    end
    return "<".. table.concat(ret,",") ..">"
  end,

  --- shortcut for G:adjString(canonOrderIR(G)).
  --- serves as a complete invariant for uncolored graphs.
  --- @param G Graph
  --- @return string
  canAdjString = function(G)
    return G:adjString(canonOrderIR(G))
  end,

  --- computes the adjacency matrix.
  --- @param G Graph
  --- @param vorder integer[] row and column order
  --- @return Matrix
  adjMatrix = function(G,vorder)
    vorder = vorder or seq(1,G:n())
    local adj, line = {}, nil
    for i,vi in ipairs(vorder) do
      line = {}
      for j,vj in ipairs(vorder) do
        line[j] = (G:hasEdge(G:vertex(vi),G:vertex(vj)) and 1) or 0
      end
      adj[i] = line
    end
    return Matrix(adj,G:n())
  end,

  --- computes the walk matrix with ncol columns.
  --- column i is defined as e*A^(i-1) where
  --- e is the filter vector.
  --- @param G Graph
  --- @param filter integer[]|Matrix filter vector
  --- @param ncol integer default: G:n()
  --- @param useClosedWalks boolean column i is diagonal of A^(i-1)
  --- @return Matrix
  walkMatrix = function(G,filter,ncol,useClosedWalks,useBigInt)
    local am = G:adjMatrix()
    if useBigInt then
      am = Matrix(am,nil,{el = function(_,n) return BigInt(n) end})
    end
    if useClosedWalks then
      return am:powerMatrixDiagonals(filter,ncol)
    else
      return am:filteredPowerMatrix(filter,--[[sym=]]true,ncol)
    end
  end,

  --- computes a string representation of the walk matrix.
  --- The rows are converted to strings first and then
  --- sorted lexicographically. Thus this constitutes an
  --- invariant (as opposed to the 'raw' walk matrix).
  --- @param G Graph
  --- @param filter integer[]|Matrix filter vector
  --- @param ncol integer default: G:n()
  --- @param useClosedWalks boolean column i is diagonal of A^(i-1)
  --- @return string
  walkMatrixSortedString = function(G,filter,ncol,useClosedWalks)
    local wm
    if useClosedWalks then
      wm = G:adjMatrix():powerMatrixDiagonals(filter,ncol)
    else
      wm = G:adjMatrix():filteredPowerMatrix(filter,--[[sym=]]true,ncol)
    end
    local wms = {}
    for i=1,#wm do
      wms[i] = table.concat(wm[i]," ")
    end
    table.sort(wms)
    return table.concat(wms,"\n")
  end,

  --- computes a partition part of the vertices such that
  --- for each s in part: u,v \in s \iff the rows corresponding
  --- to u and v in mat are equal.
  --- The sets and vertices in the sets are sorted according to
  --- vorder, see Graph.orbits for details.
  --- @param G Graph
  --- @param mat Matrix nxk-Matrix for some k
  --- @param vorder integer[]
  --- @return any[][]
  matrixPartition = function(G,mat,vorder)
    vorder = vorder or seq(1,G:n())
    local rowseen = {}
    local part = {}
    for _,v in ipairs(vorder) do
      local vrow = table.concat(mat[v]," ")
      local partn = rowseen[vrow]
      if not partn then
        partn = #part + 1
        part[partn] = {v}
        rowseen[vrow] = partn
      else
        table.insert(part[partn],v)
      end
    end
    return part
  end,

  --- computes a partition part of the vertices such that
  --- for each s in part: u,v \in s \iff the rows corresponding
  --- to u and v in the walkMatrix are equal.
  --- The sets and vertices in the sets are sorted according to
  --- vorder, see Graph.orbits for details.
  --- @param G Graph
  --- @param filter integer[]|Matrix filter vector
  --- @param vorder integer[]
  --- @param ncol integer default: G:n()
  --- @param useClosedWalks boolean column i is diagonal of A^(i-1)
  --- @return any[][]
  walkMatrixPartition = function(G,filter,vorder,ncol,useClosedWalks)
    vorder = vorder or seq(1,G:n())
    local wm
    if useClosedWalks then
      wm = G:adjMatrix():powerMatrixDiagonals(filter,ncol)
    else
      wm = G:adjMatrix():filteredPowerMatrix(filter,--[[sym=]]true,ncol)
    end
    return G:matrixPartition(wm,vorder)
  end,

  --- computes a the number of walks where each vertex has the
  --- given degree from a degree sequence for all vertices
  --- and all degree sequence of length 'length'.
  --- @param G Graph
  --- @param length integer
  --- @return DSW
  degereeSequenceWalks = function(G,length)
    local n = G:n()
    length = length or n-1
    local part = G:degPart()
    local degs = ikeys(part)
    table.sort(degs)
    local adj = G:adjMatrix()
    local adf,acf = {},{}
    for _,d in ipairs(degs) do
      --diagonal filtered by degree class
      adf[d] = Matrix.one(n):zeroFilledExcept(part[d],part[d])
      --colums filtered by degree class
      --i.e, we want a target vertex from that class
      acf[d] = adj:zeroFilledExcept(nil,part[d])
    end
    local zero = Matrix(n)
    local filter = {}
    for i=1,n do
      filter[i] = 1
    end
    filter = Matrix(filter,1)
    local work
    --- @cond IGNORE
    work = function(ds,mat)
    --- @endcond
      if mat == zero then
        return {}
      elseif #ds == length + 1 then
        local ret,sum = {},0
        mat = mat*filter
        for i=1,n do
          if mat[i][1] ~= 0 then
            table.insert(ret,{i,mat[i][1]})
            sum = sum + mat[i][1]
          end
        end
        return {{ds,sum,ret}}
      end
      local ret = {}
      for _,d in ipairs(degs) do
        local matd = mat*(acf[d])
        local retd = work(gluetables(ds,{d}),matd)
        for _,r in ipairs(retd) do
          table.insert(ret,r)
        end
      end
      return ret
    end

    local dsw = {}
    for _,d in ipairs(degs) do
      local retd = work({d},adf[d])
      for _,r in ipairs(retd) do
        table.insert(dsw,r)
      end
    end

    return setmetatable(dsw,DSW)
  end,

  --- computes a representation in DOT format, that is used by
  --- the Graphviz family of programs.
  --- https://en.wikipedia.org/wiki/DOT_%28graph_description_language%29
  --- @param G Graph
  --- @param name string used after "graph" in DOT format
  --- @return string
  toDot = function(G,name)
    name = name or "gr"
    local glines = {"graph " .. name .. " {"}
    G = G:toIntGraph()
    for e in G:edges() do
      table.insert(glines,"v"..e[1].." -- v"..e[2] ..";")
    end
    table.insert(glines,"}")
    return table.concat(glines)
  end,

  --- For an integer graph G, checks whether S is a vertex cover of G.
  --- @param G Graph
  --- @param S table<integer,boolean> S[v] == true iff v \in S
  --- @return boolean
  isVCint = function(G,S)
    for e in G:edges() do
      if (not S[e[1]]) and not S[e[2]] then
        return false
      end
    end
    return true
  end,

  --- Classic FPT vertex cover algorithm, computes a superset of
  --- S with at most k vertices that is a vertex cover (if such a
  --- set exists) for the subgraph pof G consistiung of all edges
  --- starting from the (i+1)th edge.
  --- @param G Graph
  --- @param k integer
  --- @param S table<integer,boolean> S[v] == true iff v \in S
  --- @param i integer last edge already covered by S
  --- @return integer[]|nil
  vc = function(G,k,S,i)
    if not S then
      G = G:toIntGraph()
      S = {}
      i = 0
    end
    for j=i+1,G:m() do
      local e = G:edge(j)
      if (not S[e[1]]) and not S[e[2]] then
        if k <= 0 then
          return nil
        else
          k = k - 1
          S[e[1]] = true
          local vc = G:vc(k,S,j)
          if vc then
            return vc
          end
          S[e[1]] = nil
          S[e[2]] = true
          vc = G:vc(k,S,j)
          if not vc then
            S[e[2]] = nil
          end
          return vc
        end
      end
    end
    return ikeys(S)
  end,

  --- Flips a 2-matching and returns two resulting graphs.
  --- Given to edges, it checks whether they form a matching and
  --- if so returns a pair of graphs, where each graph has one of the
  --- other two possible matchings between the four vertices
  --- involved.
  --- @param G Graph
  --- @param e1 any frist edge of matching
  --- @param e2 any second edge of matching
  --- @return Graph|nil
  --- @return Graph|nil
  match2Flip = function(G,e1,e2)
    if type(G.V == "number") then
      G = G:toIntGraph()
    end
    if  e1[1] == e2[1] or e1[1] == e2[2] or
        e1[2] == e2[1] or e1[2] == e2[2] or
        G:hasEdge(e1[1],e2[1]) or G:hasEdge(e1[1],e2[2]) or
        G:hasEdge(e1[2],e2[1]) or G:hasEdge(e1[2],e2[2]) then
      return nil
    end

    local G1,G2 = Graph(G.V,G.E),Graph(G.V,G.E)
    G1:removeEdge(e1)
    G1:removeEdge(e2)
    G1:addEdge(e1[1],e2[1])
    G1:addEdge(e1[2],e2[2])
    G2:removeEdge(e1)
    G2:removeEdge(e2)
    G2:addEdge(e1[1],e2[2])
    G2:addEdge(e1[2],e2[1])
    return G1,G2
  end,

  --- Checks wether G is a tree.
  --- Note that not all trees are of the class "Tree".
  --- @param G Graph
  --- @return boolean
  isTree = function(G)
    local n = G:n()
    local cur = G:vertex(1);
    local queue,pos={cur},1
    local parent={[cur]=true}
    while pos <= n do
      if not cur then
        return false
      end
      for v in G:neighbors(cur) do
        if not (parent[cur] == v) then
          if parent[v] then
            return false
          end
          parent[v] = cur
          table.insert(queue,v)
        end
      end
      pos=pos+1
      cur=queue[pos]
    end
    return true
  end,

  --- Checks wether G is connected.
  --- Note that not all trees are of the class "Tree".
  --- @param G Graph
  --- @return boolean
  isConnected = function(G)
    local n = G:n()
    local cur = G:vertex(1)
    local queue,pos={cur},1
    local parent={[cur]=true}
    while pos <= n do
      if not cur then
        return false
      end
      for v in G:neighbors(cur) do
        if not parent[v] then
          parent[v] = cur
          table.insert(queue,v)
        end
      end
      pos=pos+1
      cur=queue[pos]
    end
    return true
  end,

  --- Computes connected components.
  --- @param G Graph
  --- @return any[][] components
  --- @return table<any,any[]> components indexed by vertices
  conComp = function(G)
    local n = G:n()
    local vit = G:vertices()
    local cur = vit()
    local curComp = {cur}
    local comp, compbyv = {curComp}, {[cur]=curComp}
    local queue,pos={cur},1
    while pos <= n do
      if not cur then
        cur = vit()
        while compbyv[cur] do
          cur = vit()
        end
        --only needed for correct #queue value:
        queue[pos] = cur
        curComp = {cur}
        table.insert(comp,curComp)
        compbyv[cur] = curComp
      end
      for v in G:neighbors(cur) do
        if not compbyv[v] then
          compbyv[v] = curComp
          table.insert(curComp,v)
          table.insert(queue,v)
        end
      end
      pos=pos+1
      cur=queue[pos]
    end
    return comp, compbyv
  end,

  --- G[S] returns the induced subgraph with vertex set S.
  --- @param G Graph
  --- @param S any[]
  --- @return Graph
  __index = function(G,S)
    if type(S) == "string" then
       return rawget(G,S) or Graph[S]
    end
    local ret = Graph(S,{})
    S = valToKeys(S)
    for u,v in G:edges() do
      if S[u] and S[v] then
        ret:addEdge(u,v)
      end
    end
    return ret
  end,

  --- String representation using G.spec and G.adjMatrix
  --- @param G Graph
  --- @return string
  __tostring = function(G)
    return G:spec() ..":\n"..tostring(G:adjMatrix())
  end
}
makeclass(M.Graph)
Graph=M.Graph

--[[
most of the following classes are "fake", meaning that they
do not actually set the metatable of new instances to themselves
but to Graph. This allows each member to carry their own
methods, which gives a bit more flexibility and saves a few
lookups, since the inheritance mechanism from oo.lua uses
a chain of lookups during runtime.
This situation might change later for each class individually
as needed.
Currently, only Tree is a "proper" class, i.e., sets itself as
the metatable and has a method (pathMatrix) defined
class-wide.
--]]

--- (n x n) rook's graph.
--- has computed edges.
--- vertex set is {1,...,n}^2
--- edge set is {{(a,b),(c,d)}| a=b xor c=d}
--- @extends Graph
--- @class Rook : Graph
M.Rook = {
  --- constructs (n x n) rook's graph.
  --- has computed edges.
  --- vertex set is {1,...,n}^2
  --- edge set is {{(a,b),(c,d)}| a=b xor c=d}
  new = function(n)
    local G = Graph(n^2,n^2*(n-1))
    G._vertex = function(i)
      i = i-1
      local j = i % n
      i = (i-j)/n
      return {i,j}
    end
    G._vertices = function()
      local i,j = 0,-1
      return function()
        if j == n-1 then
          i = i+1
          j = 0
        else
          j= j+1
        end
        if i == n then
          return nil
        else
          return {i,j}
        end
      end
    end
    G._edges = function()
      local hv,i,j1,j2 = 0,0,0,1
      return function()
        local ret
        if hv == 0 then
          ret = {{i,j1},{i,j2}}
        elseif hv == 1 then
          ret = {{j1,i},{j2,i}}
        else
          return nil
        end
        j2 = j2 + 1
        if j2 == n then
          j1 = j1 +1
          j2 = j1 +1
        end
        if j1 == n-1 then
          i = i+1
          j1,j2 = 0,1
        end
        if i == n then
          i = 0
          hv = hv + 1
        end
        return table.unpack(ret)
      end
    end
    G._hasEdge = function(u,v)
      return (u[1]==v[1] and u[2]~=v[2])
      or (u[1]~=v[1] and u[2]==v[2])
    end
    return G
  end
}
makeclass(M.Rook)

local CaleyZnZm
--- Caley graph of group product Zn x Zm.
--- has computed edges.
--- vertex set is {0,...,n-1}x{0,...,m-1}
--- edge set is {{(a,b),(c,d)}| (a-b,d-c) \in gen}
--- @extends Graph
--- @class CaleyZnZm : Graph
M.CaleyZnZm = {
  --- constructs a Caley graph of group product Zn x Zm.
  --- has computed edges.
  --- vertex set is {0,...,n-1}x{0,...,m-1}
  --- edge set is {{(a,b),(c,d)}| (a-b,d-c) \in gen}
  new = function(n,m,gen)
    --ensure gen is symmetric:
    for i=1,#gen do
      local j,k = table.unpack(gen[i])
      j,k = (n-j)%n,(m-k)%m
      local found=false
      for h=1,#gen do
        if gen[h][1] == j and gen[h][2] == k then
          found=true
          break
        end
      end
      if not found then
        table.insert(gen,{j,k})
      end
    end
    local G = Graph(n*m,n*m*(#gen)/2)
    G._vertex = function(i)
      i = i-1
      local j = i % m
      i = (i-j)/m
      return {i,j}
    end
    G._vertices = function()
      local i,j = 0,-1
      return function()
        if j == m-1 then
          i = i+1
          j = 0
        else
          j= j+1
        end
        if i == n then
          return nil
        else
          return {i,j}
        end
      end
    end
    G._edges = function()
      local i,j,t = 0,0,1
      return function()
        local ret = {{i,j},{(i+gen[t][1])%n,(j+gen[t][2])%m}}
        if i == n then
          return nil
        end
        t = t + 1
        if t > #gen then
          j = j+1
          t = 1
          if j >= m then
            i = i+1
            j = 0
          end
        end
        return table.unpack(ret)
      end
    end
    G._hasEdge = function(u,v)
      for _,g in pairs(gen) do
        if (u[1]+g[1])%n == v[1] and (u[2]+g[2])%m == v[2] then
          return true
        end
      end
      return false
    end
    G._complement = function()
      local hasgen = {}
      hasgen[0] = {[0]=true}--fake identity gen.=> ident. not in comp.
      for i=1,#gen do
        local j,k = table.unpack(gen[i])
        hasgen[j] = hasgen[j] or {}
        hasgen[j][k] = true
      end
      local genc={}
      for j=0,div(n,2) do
        for k=0,m-1 do
          if not hasgen[j] or not hasgen[j][k] then
            table.insert(genc,{j,k})
          end
        end
      end
      return CaleyZnZm(n,m,genc)
    end
    return G
  end
}
makeclass(M.CaleyZnZm)
CaleyZnZm = M.CaleyZnZm

--- complete bipartite graph
--- has computed edges.
--- vertex set is {1,...,n+m}
--- edge set is {{i,j}| i<= n, j> n}
--- @extends Graph
--- @class BipCompl : Graph
M.BipCompl = {
  --- constructs a complete bipartite graph.
  --- has computed edges.
  --- vertex set is {1,...,n+m}
  --- edge set is {{i,j}| i<= n, j> n}
  new = function(n,m)
    local G = Graph(n+m,n*m)
    G._vertex = function(i)
      if i <= n then
        return {1,i}
      else
        return {2,i-n}
      end
    end
    G._edge = function(i)
      i = i - 1
      local j = i % m
      i = (i-j)/m+1
      j = j+1
      return {{1,i},{2,j}}
    end
    G._hasEdge = function(u,v)
      return u[1] ~= v[1]
    end
    --TODO: complement is union of cliques
--     G._complement = function()
--     end
    return G
  end
}
makeclass(M.BipCompl)


--- matching on 2n vertices
--- has computed edges.
--- vertex set is {1,2}x{1,...,n}
--- edge set is {{(1,i),(2,i)}| i<= n}
--- @extends Graph
--- @class Match : Graph
M.Match = {
  --- constucts a matching on 2n vertices
  --- has computed edges.
  --- vertex set is {1,2}x{1,...,n}
  --- edge set is {{(1,i),(2,i)}| i<= n}
  new = function(n,P)
    local G = Graph(2*n,n,false,P)
    G._vertex = function(i)
      if i <= n then
        return {1,i}
      else
        return {2,i-n}
      end
    end
    G._edge = function(i)
      return {{1,i},{2,i}}
    end
    G._hasEdge = function(u,v)
      return (u[1] ~= v[1] and u[2] == v[2])
    end
    --TODO: complement is union matchings
--     G._complement = function()
--     end
    return G
  end
}
makeclass(M.Match)

--- path with n vertices, i.e., length n-1
--- has computed edges.
--- vertex set is {1,...,n}
--- edge set is {{i,i+1}| i < n}
--- @extends Graph
--- @class Path : Graph
M.Path = {
  --- constructs a path with n vertices, i.e., length n-1
  --- has computed edges.
  --- vertex set is {1,...,n}
  --- edge set is {{i,i+1}| i < n}
  new = function(n,P)
    local G = Graph(n,n-1,false,P)
    G._vertex = function(i)
      if i <= n then
        return i
      end
    end
    G._edge = function(i)
      return {i,i+1}
    end
    G._hasEdge = function(u,v)
      return (u-v)^2 == 1
    end
    return G
  end
}
makeclass(M.Path)


--- cycle with n vertices
--- has computed edges.
--- vertex set is {1,...,n}
--- edge set is {{i,(i % n)+1}| i <= n}
--- @extends Graph
--- @class Cycle : Graph
M.Cycle = {
  --- constructs a cycle with n vertices
  --- has computed edges.
  --- vertex set is {1,...,n}
  --- edge set is {{i,(i % n)+1}| i <= n}
  new = function(n,P)
    local G = Graph(n,n,false,P)
    G._vertex = function(i)
      if i <= n then
        return i
      end
    end
    G._edge = function(i)
      return {i,(i%n)+1}
    end
    G._hasEdge = function(u,v)
      return ((u-v)^2)%n == 1
    end
    return G
  end
}
makeclass(M.Cycle)

local Tree
--- generate a (rooted) tree from a sequence of degrees.
--- i.e, like a protocol of a BFS
--- typical call: Tree(2,3,3,2,2,1,1,1,1,P)
--- also possible Tree({2,3,3,2,2,1,1,1,1},P)
--- both with and without P
--- Alternatively, Tree.fromLevelSequence accepts a sequence
--- of levels (root has level 1!)
--- @extends Graph
--- @class Tree : Graph
--- @field children table<integer,integer[]>
M.Tree = {
  --- constructor: argument is a degree sequence either as
  --- separate arguments or as a single table
  new = function(...)
    local degseq,P = {...}, nil
    local n = #degseq
    if type(degseq[1]) == "table" then--Tree({2,3,...},P)
      P = degseq[2]
      degseq = degseq[1]
      n = #degseq
    elseif type(degseq[n]) == "table" then--Tree(2,3,...,P), P ~= nil
      P = degseq[n]
      degseq[n] = nil
      n = n-1
    end
    local V,E = {1},{}
    local parent = 1
    local children = {[1]={}}
    local sibl = -1
    for v=2,n do
      V[v] = v
      --check if our potential parent already has
      --too many children
      if sibl == degseq[parent] -1 then
        parent = parent + 1
        while degseq[parent] == 1 do
          parent = parent + 1
          if not degseq[parent] then
            error("impossible degree sequence: "..
              table.concat(degseq,","))
          end
        end
        sibl = 0
        children[parent] = {}
      end
      table.insert(E,{parent,v})
      table.insert(children[parent],v)
      sibl = sibl + 1
    end
    --parent has enough children?
    assert(sibl == degseq[parent] -1,"impossible degree sequence: "..
              table.concat(degseq,","))
    --check that all remaining "parents" are actually leaves:
    for l=parent+1,n do
      assert(degseq[l] == 1,"impossible degree sequence: "..
              table.concat(degseq,","))
    end
    local T = Graph(V,E,false,P)
    T.specstr = "Tree("..table.concat(degseq,",")..")"
    setmetatable(T,Tree)
    T.children = children
    T._degrees = dcopy(degseq)
    return T
  end,

  --- similar to constructor, but uses a level sequence instead of a degree sequence.
  fromLevelSequence = function(...)
    local levseq,P = {...}, nil
    local n = #levseq
    if type(levseq[1]) == "table" then--call ({2,3,...},P)
      P = levseq[2]
      levseq = levseq[1]
      n = #levseq
    elseif type(levseq[n]) == "table" then--call (2,3,...,P), P ~= nil
      P = levseq[n]
      levseq[n] = nil
      n = n-1
    end

    local V,E = {1},{}
    local parent = {[1]=nil}
    local children = {[1]={}}
    for v=2,n do
      V[v] = v

      if levseq[v] == levseq[v-1] then--sibling
        parent[v] = parent[v-1]
        table.insert(children[parent[v]],v)
        table.insert(E,{parent[v],v})
      elseif levseq[v] > levseq[v-1] then-- first child
        parent[v] = v-1
        children[v-1] = {v}
        table.insert(E,{v-1,v})
      else--smaller level, i.e.: sibling of ancestor
        local anc = parent[v-1]
        while levseq[anc] >= levseq[v] do
          anc = parent[anc]
        end
        parent[v] = anc
        table.insert(children[anc],v)
        table.insert(E,{anc,v})
      end
    end

    local T = Graph(V,E,false,P)
    T.specstr = "TreeLevSeq("..table.concat(levseq,",")..")"
    setmetatable(T,Tree)
    T.children = children
    return T
  end,

  --- returns (and computes if necessary) the *preorder* level sequence
  --- and an ordering of the vertices to match that sequence.
  levseq = function(T,r --[[just for rec. calls:]],l,ls,ord)
    if r==1 and T._levseq then
      return T._levseq, T._levord
    end
    r = r or 1
    l = l or 1
    ls = ls or {}
    table.insert(ls,l)
    ord = ord or {}
    table.insert(ord,r)
    for _,ch in ipairs(T.children[r] or {}) do
      T:levseq(ch,l+1,ls,ord)
    end
    if r == 1 then
      T._levseq, T._levord = ls, ord
    end
    return setmetatable(ls,LS), ord
  end,

  --- returns (and computes if necessary) the BFS degree sequence
  --- and an ordering of the vertices to match that sequence.
  degseq = function(T,r)
    if r==1 and T._levseq then
      return T._levseq, T._levord
    end
    local deg = T:degrees()--degrees in *current* order
    local ds,ord = setmetatable({},DS),{}
    local queue = {}
    r = r or 1
    local v = r
    local i = 0
    while v do
      table.insert(ord,v)
      table.insert(ds,deg[v])
      for _,ch in ipairs(T.children[v] or {}) do
        table.insert(queue,ch)
      end
      i = i+1
      v = queue[i]
    end
    if r == 1 then
      T._degseq, T._degord = ds, ord
    end
    return ds, ord
  end,

  --- Turns a level sequence into a degree sequence **without**
  --- creating a tree first.
  --- @param ls integer[]
  --- @return DS
  levseqToDegseq = function(ls,...)
    if type(ls) ~= "table" then
      ls = {ls,...}
    end
    local deg = {[1]=0}
    local levels = {[1]={1}}
    local parent = {[1]=nil}
    local par = nil
    local l,lpre = nil,1

    for v=2,#ls do
      l = ls[v]
      levels[l] = levels[l] or {}
      table.insert(levels[l],v)
      deg[v] = 1--parent is first neighbor
      if l > lpre then-- first child
        par = v-1
        parent[v] = par
      elseif l < lpre then--smaller level, i.e.: sibling of ancestor
        while ls[par] >= l do
          par = parent[par]
        end
      end
      --else l == lpre -> sibling -> par remains
      parent[v] = par
      deg[par] = deg[par] + 1
      lpre = l
    end

    local i,ds = 0, {}
    for _,verts in ipairs(levels) do
      for _,v in ipairs(verts) do
        i = i + 1
        ds[i] = deg[v]
      end
    end

    return setmetatable(ds,DS)
  end,

  --- returns an iterator over all non-isomorphic
  --- **rooted** trees (naive version, just for reference).
  --- according to https://doi.org/10.1137/0209055
  --- naive implementation of the successor function
  --- that creates a new sequence for each run
  --- naive implementation takes 13s for n=20
  --- optimized s.b. takes 1s
  --- @param n integer
  --- @return function
  rootedCanLevSeqNaive = function(n)
    local succ = setmetatable(seq(1,n),LS)
    return function()
      if not succ then
        return nil
      end
      local cur = succ
      local p = n
      while p ~= 1 and cur[p] == 2 do
        p = p-1
      end
      if p == 1 then
        succ = nil
        return cur
      end

      local lp,q = cur[p],p-1
      while cur[q] > lp-1 do
        q = q-1
      end
      local d = p-q
      succ = setmetatable({},LS)
      for i=1,p-1 do
        succ[i] = cur[i]
      end
      for i=p,n do
        succ[i] = succ[i-d]
      end

      return cur
    end
  end,

  --- returns an iterator over all non-isomorphic
  --- **rooted** trees (implementation close to paper).
  --- according to
  --- [https://doi.org/10.1137/0209055](https://doi.org/10.1137/0209055)
  --- uses that p cannot decrease by more than two
  --- and is n or n-1 if d > 1
  --- (because "2,2" cannot appear before p)
  --- does NOT copy sequences (would be pointless)
  --- @param n integer
  --- @return function
  rootedCanLevSeq = function(n)
    local p = n ~= 2 and n or 1
    local pre, prepre = seq(1,p-1), {}

    local first = true
    local ls = setmetatable(seq(1,n),LS)
    return function()
      if first then
        first = false
        return ls
      end
      if p == 1 then--last ls was 1,2,2,...
        return nil
      end

      --q is always parent of p:
      local lsq = ls[p] - 1

      --If ls[p] *will be* 2 and ls[p-1] is already 2,
      --ls[i] is *and will be* 2 -> thus no copying required and
      --p will not increase
      if p<n and (lsq ~= 2 or ls[p-1] ~= 2) then
        local q = pre[lsq]
        local d = p-q
        for i=p,n-1 do
          ls[i] = ls[i-d]
          prepre[i] = pre[ls[i]]
          pre[ls[i]] = i
        end
        ls[n] = ls[n-d]
      else
        ls[p] = lsq--only update ls[p] to ls[q]
      end

      p=n
      --runs max. 2 times:
      while ls[p] == 2 do
        p = p-1
        pre[ls[p]] = prepre[p]
      end

      return ls
    end
  end,

  --- analogous to the walk matrix, just for paths
  --- Since we consider trees, k vertices in distance d from v
  --- imply exactly k paths of length d from v.
  --- @param T Tree
  --- @param filter Matrix|table
  --- @param maxDist integer maximal distance to consider (default T:n()-1)
  --- @return Matrix
  pathMatrix = function(T,filter,maxDist)
    local n = T:n()
    maxDist = maxDist or n-1
    local d = Matrix(n)
    local queue = {1}
    while #queue > 0 do
      local parent = table.remove(queue,1)
      for _,ch in ipairs(T.children[parent] or {}) do
        d[parent][ch],d[ch][parent] = 1,1
        for i=1,n do
          if d[parent][i] ~= 0 and i ~= ch then
            local dist = d[parent][i]+1
            dist = (dist > maxDist and -1) or dist
            d[i][ch],d[ch][i] = dist,dist
          end
        end
        table.insert(queue,ch)
      end
    end

    --- @cond IGNORE
    local onlywhere = function(mat,l)
    --- @endcond
      local ret = Matrix(mat)
      for i=1,n do for j = 1,n do
        ret[i][j] = ((ret[i][j] == l) and 1) or 0
      end end
      return ret
    end

    if not filter then
      filter = {}
      for i=1,n do
        filter[i] = 1
      end
    end
    filter = Matrix(filter,1)--column vector
    local pm = filter
    for l=1,maxDist do
      pm = pm:glue(onlywhere(d,l)*filter)
    end
    return pm
  end,

  --- computes a partition part of the vertices such that
  --- for each s in part: u,v \in s \iff the rows corresponding
  --- to u and v in the pathMatrix are equal.
  --- The sets and vertices in the sets are sorted according to
  --- vorder, see Graph.orbits for details.
  --- @param T Tree
  --- @param filter integer[]|Matrix filter vector
  --- @param vorder integer[]
  --- @param ncol integer default: G:n()
  --- @return any[][]
  pathMatrixPartition = function(T,filter,vorder,ncol)
    vorder = vorder or seq(1,T:n())
    local pm = T:pathMatrix(filter,ncol)
    return T:matrixPartition(pm,vorder)
  end,

  --- Similar to a dendrogram.
  --- @param T Tree named self
  --- @param ml boolean multiline
  --- @return string
  layout = function(T,ml)
    local rec
    --- @cond IGNORE
    rec = function(r,ind)
    --- @endcond
      local lout = r
      if T.children[r] then
        ind = ml and (ind and ("   " .. ind) or " └─") or ""
        lout = lout ..(ml and "\n" or "[")
        for _,ch in ipairs(T.children[r]) do
          lout = lout .. ind .. rec(ch,ind) .. ((ml and "\n") or ",")
        end
        lout = lout:sub(1,#lout-1)..(ml and "" or "]")
      end
      return lout
    end
    return rec(1)
  end,
}
--"proper" subclass, see setmetatable call
makeclass(M.Tree,{M.Graph})
Tree = M.Tree


M.namedGraphs = {
  rooks44 = M.Rook(4),
  shrikhande = M.CaleyZnZm(4,4,{{0,1},{1,0},{1,1}}),
  k33 = M.BipCompl(3,3):toIntGraph(),
  k23 = M.BipCompl(2,3):toIntGraph(),
  m3 = M.Match(3):toIntGraph(),
  m4 = M.Match(4):toIntGraph(),
  m5 = M.Match(5):toIntGraph(),
  m6 = M.Match(6):toIntGraph(),
  p3 = M.Path(3):toIntGraph(),
  p4 = M.Path(4):toIntGraph(),
  p5 = M.Path(5):toIntGraph(),
  p6 = M.Path(6):toIntGraph(),
  p7 = M.Path(7):toIntGraph(),
  p8 = M.Path(8):toIntGraph(),
  p9 = M.Path(9):toIntGraph(),
  p10 = M.Path(10):toIntGraph(),
  p11 = M.Path(11):toIntGraph(),
  p12 = M.Path(12):toIntGraph(),
  p13 = M.Path(13):toIntGraph(),
  p14 = M.Path(14):toIntGraph(),
  p15 = M.Path(15):toIntGraph(),
  p16 = M.Path(16):toIntGraph(),
}

return M
