--- read and write nauty's graph6 format
local oo = require "oo"
local div = oo.div
local dcopy,perm = oo.dcopy,oo.perm
local graphs = require "graphs"
local Graph = graphs.Graph
local Path = graphs.Path

local M = {
  MODINFO = {
    name = "graph6",
    links = {}
  }
}

--- computes a 6-bit MSB table representation of integer i.
local intToBit6 = function(i)
  local t = {}
  t[6] = i % 2
  i = (i-t[6])/2
  t[5] = i % 2
  i = (i-t[5])/2
  t[4] = i % 2
  i = (i-t[4])/2
  t[3] = i % 2
  i = (i-t[3])/2
  t[2] = i % 2
  i = (i-t[2])/2
  t[1] = i % 2
  return t
end

--- computes a b-bit MSB table representation of integer i.
local intToBits = function(i,b)
  local t = {}
  for p = b,1,-1 do
    t[p] = i % 2
    i = (i-t[p])/2
  end
  return t
end

--- inverse of intToBit6
local bit6toInt = function(t)
  return t[1]*32+t[2]*16+t[3]*8+t[4]*4+t[5]*2+t[6]*1
end

--- inverse of intToBits
local bitsToInt = function(t)
  local i = 0
  for p=1,#t do
    i = 2*i + t[p]
  end
  return i
end

local Renc = function(t)
  t = dcopy(t)
  for i=#t+1,#t+((-#t)%6) do
    t[i] = 0
  end
  local a = {}
  for i=0,#t-1,6 do
    local x = 63+t[i+1]*32+t[i+2]*16+t[i+3]*8+t[i+4]*4+t[i+5]*2+t[i+6]*1
    table.insert(a,string.char(x))
  end
  return table.concat(a)
end

local Rdec = function(s)
  local bytes = {s:byte(1,#s)}
  local t,bits = {}, nil

  for i=1,6*#bytes do
    if i % 6 == 1 then
      bits = intToBit6(bytes[div(i-1,6)+1]-63)
    end
    t[i] = bits[(i-1) % 6 + 1]
  end
  return t
end

local Nenc = function(n)
  if n <= 62 then
    return string.char(63+n)
  elseif n <= 258047 then
    return string.char(126) .. Renc(intToBits(n,18))
  elseif n <= 68719476735 then
    return string.char(126) .. string.char(126) ..
        Renc(intToBits(n,36))
  end
end

local Ndec = function(s)
  if s:byte(1) ~= 126 then
    return s:byte(1) - 63, s:sub(2)
  elseif s:byte(2) ~= 126 then
    return bitsToInt(Rdec(s:sub(2,4))), s:sub(5)
  elseif s:byte(3) ~= 126 then
    return bitsToInt(Rdec(s:sub(3,8))), s:sub(9)
  end
end

M.g6ToGraph = function(g6)
  local p,n = 0, nil
  n,g6 = Ndec(g6)
  g6 = Rdec(g6)
  local V, E = (n>0 and {1}) or {},{}
  for j=2,n do
    V[j] = j
    for i=1,j-1 do
      p = p+1
      if g6[p] == 1 then
        table.insert(E,{i,j})
      end
    end
  end
  return Graph(V,E)
end
local g6ToGraph = M.g6ToGraph

M.graphToG6 = function(G)
  local n,p = G:n(),0
  local g6 = {}
  for j=2,n do
    for i=1,j-1 do
      p = p+1
      g6[p] = (G:hasEdge(G:vertex(i),G:vertex(j)) and 1) or 0
    end
  end
  return Nenc(G:n()) .. Renc(g6)
end

M.graphToDreadnaut = function(G)
  local n = G:n()
  local dn = "n=" .. n .. " $=1 g\n"
  for i=1,n do
    dn = dn .. " " .. i .. ": "
    for j=i+1,n do
      dn = (G:hasEdge(G:vertex(i),G:vertex(j)) and (dn .. j .. " ")) or dn
    end
    dn = dn .. ";\n"
  end
  return dn;
end

--- Converts 2 graphs to a single graph in dreadnaut format.
--- Assumes that G:n()==H:n()
--- @return string dreadnaut string for graph with 2n+2 vertices
M.twoGraphsToDreadnaut = function(G,H)
  local n = G:n()
  local dn = "n=" .. 2*n+2 .. " $=1 g\n"
  for i=1,n do
    dn = dn .. " " .. i .. ": "
    for j=i+1,n do
      dn = (G:hasEdge(G:vertex(i),G:vertex(j)) and (dn .. j .. " ")) or dn
    end
    dn = dn .. ";\n"
  end
  for i=1,n do
    dn = dn .. " " .. (n+i) .. ": "
    for j=i+1,n do
      dn = (H:hasEdge(H:vertex(i),H:vertex(j)) and (dn .. n+j .. " ")) or dn
    end
    dn = dn .. ";\n"
  end

  --connect all vertices of each graph to a new vertexs
  dn = dn .. " " .. (2*n+1) .. ": "
  local dnll = " " .. (2*n+2) .. ": "
  for j=1,n do
    dn = dn .. j .. " "
    dnll = dnll .. n+j .. " "
  end
  dn = dn .. ";\n" .. dnll .. ";\n"

  return dn;
end

--- run dreadnaut with a given graph in dreadnaut format
M.dreadnaut = function(dn)
  local tmp = os.tmpname()
  local dnin = io.open(tmp,"w")
  assert(dnin,"could not open tmpfile " .. tmp)
  dnin:write(dn .. "\n-a\n-m\nx\no")
--   dnin:write("\n-f\nx\no")
  dnin:close()
  local dnpr = io.popen("dreadnaut < '" .. tmp .."'")
  assert(dnpr,"could not communicate to dreadnaut about tmpfile " .. tmp)
  local ret = dnpr:read("*a")
  dnpr:close()
  os.remove(tmp)
  return ret
end


local compareAdj = function(a1,a2)
  if not a1 then
    return a2
  end
  for i=1,#a1 do
    if a1[i] > a2[i] then
      return a2
    elseif a1[i] < a2[i] then
      return a1
    end
  end
  return a2
end

--- returns the lexicophraphical minimal graph6 repr. of G
M.graphToG6Mini = function(G)
  G = G:toIntGraph()
  local n = G:n()
  local g6mini
  for pi in perm(G.V) do
    local H = G:permuted(pi)
    local g6 = {}
    local p = 0
    for j=2,n do
      for i=1,j-1 do
        p = p+1
        g6[p] = (H:hasEdge(H:vertex(i),H:vertex(j)) and 1) or 0
      end
    end
    g6mini = compareAdj(g6mini,g6)
  end
  return Nenc(G:n()) .. Renc(g6mini)
end

--- reads a list of Graphs in g6 notation.
--- @param path File in g6 format
--- @param skip number of lines to skip (e.g. header)
--- @param last last line to read (incl. skipped lines)
--- @return Graph[]
M.readG6List = function(path,skip,last)
  local file = io.open(path,"r")
  assert(file,"could not open ".. path)
  local line = file:read()
  skip = skip or 0
  local l = 1
  while l <= skip and line do
    line = file:read()
    l = l+1
  end
  local list = {}
  while line and (not last or last >= l) do
    table.insert(list,g6ToGraph(line))
    line = file:read()
    l = l+1
  end
  file:close()
  return list
end
local readG6List = M.readG6List

--- reads a directory of files with lists in g6 notation.
--- A file dirlist.txt is expected which contains all files
--- in order. You may at a README file etc. to the directory
--- withlout listing it in dirlist.txt.
--- @param dir directory to read from
--- @return Graph[]
M.readG6Dir = function(dir)
  local file = io.open(dir.."/dirlist.txt","r")
  assert(file,"could not open ".. dir.."/dirlist.txt")
  local line = file:read()
  local list = {}
  while line do
    local flist = readG6List(dir.."/"..line)
    for _,G in ipairs(flist) do
      table.insert(list,G)
    end
    line = file:read()
  end
  file:close()
  return list
end

--- G6 strings of certain fixed graphs
M.namedG6 = {
  m3 = "E@Q?",
  m4 = "G?CaC?",
  p3 = "BW",
  p4 = "CL",
  p5 = "DBg",
  p6 = "E@U_",
  p7 = "",
  p8 = Path(8):toIntGraph(),
  p9 = Path(9):toIntGraph(),
}


return M



