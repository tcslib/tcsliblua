--- big integers
--- @defmodule bigint
local oo = require "oo"
local makeclass = oo.makeclass
local fillWith = oo.fillWith

local M = {
  MODINFO = {
    name = "bigint",
    links = {}
  }
}

--forward declaration
local BigInt

local div = oo.div

--least significant bit and index from 0 bis r
local binRepr = function(x)
  local e,r = {},-1
  repeat
    r=r+1
    e[r] = (x % 2 == 1) and 1 or 0
    x = div((x - e[r]),2)
  until x == 0
  return e,r
end

local iszero = function(a)
  local mt = getmetatable(a) or {}
  if mt.iszero then
    return mt.iszero()
  else
    return a == 0
  end
end

local BIGINTITEMSIZE=16777216--24bit => multiplication result
--has at most 48bits (< 56bits for Lua 5.2 and < 64bits for Lua >= 5.3)

--implementation detail -> might change:
local uint24ToBytes = function(i)
  local b1,b2,b3
  b3 = i % 256
  i = div((i-b3),256)
  b2 = i % 256
  b1 = div((i-b2),256)
  assert(b1 >=0 and b1<=255 and b2 >=0 and b2<=255 and b3 >=0 and b3<=255,
  "out of range:"..b1..":"..b2..":"..b3)
  return string.char(b1,b2,b3)
end

local bytesToUint24 = function(s)
  local b1,b2,b3 = s:byte(1,3)
  return 65536*b1+256*b2+b3
end


--not efficient, but rarely needed
local ufloatToBytes = function(i)
  s = ""
  repeat
    s = s .. string.char(i%256)
    i = div(i,256)
  until i<=0
  while #s % 3 ~= 0 do
    s = s .. "\0"
  end
  return s:reverse()
end


woLeadingZeros = function(s)
  local i = 1
  while s:sub(i,i+2) == "\0\0\0" do
    i = i+3
  end
  if i >= #s then
    return "\0\0\0"
  else
    return s:sub(i)
  end
end

--just for positive b,b2:
--returns (via binary search) max. 24-bit word fact, such that
--b>=fact*b2
local bisectMult = function(b,b2)
  local step = 256*256*128
  local fact,prod,lt,e = 0,0,false,""
  while step >= 1 do
    prod = (fact+step)*b2
    lt,e = prod:__lt(b)
    if lt or (e=="eq") then
      fact = fact + step
      if e=="eq" then
        return fact, prod
      end
    end
    step = div(step,2)
  end
  --prod will be recomputed, if <= was wrong during the last step
  return fact, ((lt or (e=="eq")) and prod) or ((fact+step)*b2)
end

--helper class for printing
local bigUInt10
local UINT10ITEMSIZE=100000000
bigUInt10 = {
  new = function(init)
    if getmetatable(init) == bigUInt10 then
      return init
    else
      assert(type(init)=="number" and init < UINT10ITEMSIZE)
      return setmetatable({init},bigUInt10)
    end
  end,

  __add = function(b,b2)
    b,b2 = bigUInt10(b),bigUInt10(b2)
    if #b < #b2 then
      b,b2 = b2,b
    end
    --length of b is now at least the length of b2
    local ldiff = #b-#b2--length difference (in 10^7 words)
    local sum = {}
    for i=1,ldiff do
      sum[i] = 0
    end
    for i=1,#b2 do
      sum[ldiff+i] = b2[i]
    end
    local carry=0
    for i=#sum,1,-1 do
      sum[i] = sum[i] + b[i] + carry
      if sum[i] >= UINT10ITEMSIZE then
        sum[i] = sum[i] % UINT10ITEMSIZE
        carry = 1
      else
        carry = 0
      end
    end
    if carry == 1 then
      for i=#sum,1,-1 do
      sum[i+1] = sum[i]
      end
      sum[1] = 1
    end
    setmetatable(sum,bigUInt10)
    return sum
  end,

  __mul = function(b,b2)
    b,b2 = bigUInt10(b),bigUInt10(b2)
    --new number of places is at most the sum of the old numbers
    local n,prod = #b+#b2,{}
    for i=1,n do
      prod[i] = 0
    end
    local jcarry,icarry = 0,0
    for iv=0,#b-1 do
      local i = #b-iv
      for jv=0,#b2-1 do
        local j = #b2-jv
        prod[i+j] = b[i]*b2[j]+prod[i+j]+jcarry
        local pmod = prod[i+j] % UINT10ITEMSIZE
        jcarry = div((prod[i+j]-pmod),UINT10ITEMSIZE)
        prod[i+j] = pmod
      end
      prod[i] = prod[i]+jcarry+icarry
      jcarry = 0
      local pmod = prod[i] % UINT10ITEMSIZE
      icarry = div((prod[i]-pmod),UINT10ITEMSIZE)
      prod[i] = pmod
    end

    assert(icarry==0)
    local nzero = 0
    while prod[nzero+1] == 0 do
      nzero = nzero+1
    end
    for i=1,#prod-nzero do
      prod[i] = prod[i+nzero]
    end
    for i=#prod-nzero+1,#prod do
      prod[i] = nil
    end
    setmetatable(prod,bigUInt10)
    return prod
  end,

  __tostring = function(b)
    local s = {tostring(b[1])}
    for i=2,#b do
      local si = tostring(b[i])
      s[i] = fillWith("","0",8-#si)..si
    end
    return table.concat(s)
  end
}
makeclass(bigUInt10)

M.BigInt = {
  new = function(init)
    local b = {}
    setmetatable(b,BigInt)
    if type(init) == "number" then
      if init < 0 then
        b.sign = -1
        init = -init
      else
        b.sign = 1
      end
      b.bytes = ufloatToBytes(init)
    elseif type(init) == "string" then
      if init:sub(1,1) == "-" then
        b.sign = -1
        init = init:sub(2)
      else
        b.sign = 1
      end
      b.bytes  = "\0"--Wert 0
      assert(#init>0,"BigInt: init mustr have length >= 1 (without sign)")
      local st,ed = 0,0
      repeat
        st = ed+1
        ed = math.min(#init,st+8)--9 decimal places are roighly 32 bit
        local mult = 10^(1+ed-st)
        b = b * mult + tonumber(init:sub(st,ed))
      until ed >= #init
    elseif init.bytes then
      b.sign = init.sign or 1
      b.bytes = init.bytes
    end
    return b
  end,

  iszero = function(b)
    return #b == 1 and b[1] == 0
  end,

  isone = function(b)
    return #b == 1 and b[1] == 1
  end,

  --b[i] where i>=1 means ith word (like in RAM, most sign. word)
  --if i <= 0 return 24-bit word with value (2^24)^(-i)
  __index = function(b,i)
    if type(i) ~= "number" then
      if BigInt[i] then
        return BigInt[i]
      else
        return rawget(b,i)
      end
    end
    if i<1 then
      i = (div(#b.bytes,3))+i
    end
    if 3*i > #b.bytes then
      return nil
    end
    return bytesToUint24(b.bytes:sub(3*(i-1)+1,3*(i-1)+3))
  end,

  __newindex = function(b,i,v)
    if type(i) ~= "number" then
      rawset(b,i,v)
    else
      error("words of BigInts are read-only")
    end
  end,

  __len = function(b)
    return (#b.bytes/3)
  end,

  log2 = function(b)
    return math.floor((#b-1)*24+math.log(b[1])/math.log(2))
  end,

  __unm = function(b)
    local mb = BigInt(b)
    mb.sign = mb.sign*(-1)
    return mb
  end,

  __lt = function(b,b2)
    b,b2 = BigInt(b),BigInt(b2)
    if b.sign < b2.sign then
      return true
    elseif b.sign > b2.sign then
      return false
    elseif b.sign == -1 then
      return (-b2):__lt(b)
    elseif #b.bytes < #b2.bytes then
      return true
    elseif #b.bytes > #b2.bytes then
      return false
    end
    for i=1,#b.bytes do
      if b.bytes:byte(i) < b2.bytes:byte(i) then
        return true
      elseif b.bytes:byte(i) > b2.bytes:byte(i) then
        return false
      end
    end
    return false, "eq"
  end,

  __le = function(b,b2)
    local lt,e = b:__lt(b2)
    return (e == "eq") or lt
  end,

  __eq = function(b,b2)
    local _,e = b:__lt(b2)
    return e == "eq"
  end,

  __add = function(b,b2)
    b,b2 = BigInt(b),BigInt(b2)
    if b.sign == 1 and b2.sign == -1 then
      return b-(-b2)
    elseif b.sign == -1 and b2.sign == 1 then
      return b2-(-b)
    end
    --signs are equal from now on
    if #b.bytes < #b2.bytes then
      b,b2 = b2,b
    end
    --length of b is now at least the length of b2
    local ldiff = #b-#b2--length difference (in 24-bit words)
    local sum = {}
    for i=1,ldiff do
      sum[i] = 0
    end
    for i=1,#b2 do
      sum[ldiff+i] = b2[i]
    end
    local carry=0
    for i=#sum,1,-1 do
      sum[i] = sum[i] + b[i] + carry
      if sum[i] >= BIGINTITEMSIZE then
        sum[i] = sum[i] % BIGINTITEMSIZE
        carry = 1
      else
        carry = 0
      end
      sum[i] = uint24ToBytes(sum[i])
    end
    local carryPre = ""
    if carry == 1 then
      carryPre = uint24ToBytes(1)
    end
    local ret={bytes=carryPre..table.concat(sum),sign=b.sign}
    setmetatable(ret,BigInt)
    return ret
  end,

  __sub = function(b,b2)
    b,b2 = BigInt(b),BigInt(b2)
    if (b.sign == 1 and b2.sign == -1) or
        (b.sign == -1 and b2.sign == 1) then
      return b+(-b2)
    end
    --signs are equal from now on
    local signFlip = 1
    if (b.sign == 1 and b < b2) or (b.sign == -1 and b > b2) then
      b,b2 = b2,b
      signFlip = -1
    end
    --now length of |b| is at least length of b2.
    local ldiff = #b-#b2--length difference (in 24-bit words)
    local diff = {}
    for i=1,ldiff do
      diff[i] = 0
    end
    for i=1,#b2 do
      diff[ldiff+i] = b2[i]
    end
    local carry=0
    for i=#diff,1,-1 do
      diff[i] = b[i] - diff[i] - carry
      if diff[i] < 0 then
        diff[i] = diff[i] % BIGINTITEMSIZE
        carry = 1
      else
        carry = 0
      end
      diff[i] = uint24ToBytes(diff[i])
    end
    assert(carry==0)
    local ret={bytes=woLeadingZeros(table.concat(diff)),
      sign=b.sign*signFlip}
    setmetatable(ret,BigInt)
    return ret
  end,

  __mul = function(b,b2)
    b,b2 = BigInt(b),BigInt(b2)
    --new number of places is at most the sum of the old numbers
    local n,prod = #b+#b2,{}
    for i=1,n do
      prod[i] = 0
    end
    local jcarry,icarry = 0,0
    for iv=0,#b-1 do
      local i = #b-iv
      for jv=0,#b2-1 do
        local j = #b2-jv
        --Index with -, see __index
        prod[i+j] = b[-iv]*b2[-jv]+prod[i+j]+jcarry
        local pmod = prod[i+j] % BIGINTITEMSIZE
        jcarry = div((prod[i+j]-pmod),BIGINTITEMSIZE)
        prod[i+j] = pmod
      end
      prod[i] = prod[i]+jcarry+icarry
      jcarry = 0
      local pmod = prod[i] % BIGINTITEMSIZE
      icarry = div((prod[i]-pmod),BIGINTITEMSIZE)
      prod[i] = pmod
    end
    assert(icarry==0)
    for i=1,n do
      prod[i] = uint24ToBytes(prod[i])
    end
    local ret = {sign=b.sign*b2.sign,
      bytes=woLeadingZeros(table.concat(prod))}
    setmetatable(ret,BigInt)
    return ret
  end,


  --- __div = __idiv.
  __idiv= function(b,b2)
    assert(not iszero(b2),"BigInt: division by 0")
    b,b2 = BigInt(b),BigInt(b2)
    local sign,bsign=b.sign*b2.sign,b.sign--b.sign is used for mod
    b = (b.sign == 1 and b) or -b
    b2 = (b2.sign == 1 and b2) or -b2
    --both are positive now, i.e., "<" compare abs. values
    local lt,e = b:__lt(b2)
    if e == "eq" then
      return BigInt(1), BigInt(0)
    elseif lt then
      return BigInt(0), (bsign==1 and b) or b2 - b
    end
    local quot,divi = {},BigInt({bytes=""})
    for i=1,#b do
      divi.bytes = woLeadingZeros(divi.bytes ..
        b.bytes:sub(3*(i-1)+1,3*(i-1)+3))
      if divi < b2 then
        quot[i] = "\0\0\0"
      else
        local fact,prod = bisectMult(divi,b2)
        quot[i] = uint24ToBytes(fact)
        divi = divi - prod
      end
    end
    divi = (bsign == 1 and divi) or b2 - divi
    return BigInt({sign=sign,
      bytes=woLeadingZeros(table.concat(quot))}),divi
  end,

  __mod = function(b,b2)
    local _,ret = BigInt.__idiv(b,b2)
    return ret
  end,

  __pow = function(b,ex)
    if ex == 0 then
      return BigInt(1)
    end
    local e,r = binRepr(ex)
    local z = b
    for i=r-1,0,-1 do
      z = z*z
      if e[i] == 1 then
        z = z*b
      end
    end
    return z
  end,

  --- naive implementation by division.
  __tostringDirect = function(b)
    local pot10,n = 10000000,7
    local dec,deci,i = "",nil,0
    while b > (pot10-1) do
      i=i+1
      b,deci= b:__idiv(pot10)
      deci = tostring(deci[1])
      dec = fillWith("","0",n-#deci)..deci..dec
    end
    dec = tostring(b[1])..dec
    return ({"",[-1]="-"})[b.sign]..dec
  end,

  --- use Horner scheme and base 10^8 bigInts internally.
  __tostring = function(b)
    local bim = bigUInt10(BIGINTITEMSIZE)
    local b10 = bigUInt10(b[1])
    for i=2,#b do
      b10 = b10*bim+b[i]
    end
    return b10:__tostring()
  end,

  tohexstring = function(b)
    local hex = {string.format("0x%X",b[1])}
    for i = 2,#b do
      hex[i] = string.format("%X",2^24+b[i]):sub(2)
    end
    return ({"",[-1]="-"})[b.sign] .. table.concat(hex)
  end
}
makeclass(M.BigInt)
BigInt=M.BigInt
BigInt.__div = BigInt.__idiv
BigInt.ths = BigInt.tohexstring


return M



