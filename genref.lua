--- generalized refinement
--- @module genref
local oo = require "oo"
local makeclass = oo.makeclass
local graphs = require "graphs"

local M = {
  MODINFO = {
    name = "genref",
    links = {}
  }
}

--- We plan to use color[t] for, e.g., t={3,8,7} in 3-WL
--- Internally color[t] is stored at
--- color.indexByTuple[3][8][7]
--- Note that refmain never assigns a new table to color.
M.indexByTuple = {
  __index = function(t,i)
    if type(i) ~= "table" then
      return rawget(t,i)
    end
    local val = rawget(t,".indexByTuple")
    for _,j in ipairs(i) do
      val = val[j]
    end
    return val
  end,

  __newindex = function(t,i,val)
    if type(i) ~= "table" then
      return rawset(t,i,val)
    end
    local parent = rawget(t,".indexByTuple")
    if not parent then
      parent = {}
      rawset(t,".indexByTuple",parent)
    end
    local ch
    for l=1,#i-1 do
      local j=i[l]
      ch = parent[j]
      if not ch then
        ch = {}
        parent[j] = ch
      end
      parent = ch
    end
    parent[i[#i]]=val
  end
}
local indexByTuple = M.indexByTuple

local colorDict
M.colorDict = {
  new = function(init)
    init = init or {}
    return setmetatable(init,colorDict)
  end,

  ---
  --- @return boolean d1 stands before d2 or is equiv. to d2
  --- @return number r: d1,d2 can be separated in round r
  __lt = function(d1,d2)
    for r=0,math.max(#d1,#d2) do
      --check that #rounds until stable match
      local d1r,d2r = d1[r],d2[r]
      if not d1r then
        return true, r
      end
      if not d2r then
        return false, r
      end

      --check that #colors match for each round
      if #d1r < #d2r then
        return true, r
      elseif #d1r > #d2r then
        return false, r
      end
      for i=0,#d1r do
        local d1ri,d2ri = d1r[i],d2r[i]
        --compare colors
        if d1ri.c < d2ri.c then
          return true, r
        elseif d1ri.c > d2ri.c then
          return false, r
        end
        --compare their multiplicities
        if d1ri.m < d2ri.m then
          return true, r
        elseif d1ri.m > d2ri.m then
          return false, r
        end
      end
    end
    --second true for equality (s.b.)
    return true, nil
  end,

  __eq = function(d1,d2)
    --no round r to distinguish them
    return not select(2,colorDict.__lt(d1,d2))
  end,

  rdist = function(d1,d2)
    return select(2,colorDict.__lt(d1,d2))
  end,

  __tostring = function(d)
    local roundsStr = {}
    for r=0,#d do
      local rstr = {"r="..r..": "..d[r][0].c.."["..d[r][0].m .."]"}
      for i,col in ipairs(d[r]) do
        rstr[i+1] = col.c.."["..col.m .."]"
      end
      roundsStr[r+1] = table.concat(rstr,"   ")
    end
    return table.concat(roundsStr,"\n")
  end
}
colorDict = M.colorDict
oo.makeclass(colorDict)

M.refmain = function(vertices,tuples,refcolor,color,k,maxr,graphFromTuple,doprint)
  graphFromTuple = graphFromTuple or function() return 1 end
  local info = ((doprint or M.print) and print) or (function() end)
  local k = k or #tuples[1]
  local nc,nclast = 0,0 --current and last iteration's number of colors

  --save big colors:
  local cd0 = {}
  local cd = colorDict({[0]=cd0})

  local colors = {}
  for _,t in pairs(tuples) do
    table.insert(colors,color[t])
  end
  table.sort(colors)
  local invColors = {}
  for _,col in ipairs(colors) do
    if not invColors[col] then
      invColors[col] = nc
      cd0[nc] = {c=col,m=1}
      nc = nc + 1
    else
      cd0[nc-1].m = cd0[nc-1].m+1
    end
  end
  info("# of colors initially:     ",nc)

  for _,t in pairs(tuples) do
    color[t] = invColors[color[t]]
  end

  local r=0
  while nc ~= nclast and (not maxr or r < maxr) do
    local rdist = nil
    r=r+1
    local lastcol = color
    color = {}
    --copy the metatable (this copies __index and __newindex)
    setmetatable(color,getmetatable(lastcol))
    local colors = {}
    local colorsByGraph = {}
    for _,t in pairs(tuples) do
      local gr = graphFromTuple(t)
      color[t] = refcolor(t,vertices,tuples,lastcol,k)
      table.insert(colors,color[t])
      colorsByGraph[gr] = colorsByGraph[gr] or {}
      table.insert(colorsByGraph[gr],color[t])
    end

    table.sort(colorsByGraph[1])
    local grOneColor = table.concat(colorsByGraph[1],";;")
    for gr,col in ipairs(colorsByGraph) do
      table.sort(colorsByGraph[gr])
      local grCol = table.concat(colorsByGraph[gr],";;")
      if grCol ~= grOneColor then
        rdist = r
        info("Graph "..gr.." distinguished from first graph in round "..r)
      end
    end

    table.sort(colors)
    local cdr = {}
    cd[r] = cdr
    nclast=nc
    nc = 0
    local invColors = {}
    for _,col in ipairs(colors) do
      if not invColors[col] then
        invColors[col] = nc
        --multiplicity is initally one in color dict.
        cdr[nc] = {c=col,m=1}
        nc = nc + 1
      else
        --increase multiplicity in color dict.
        cdr[nc-1].m = cdr[nc-1].m+1
      end
    end
    info("# of colors after round " ..r..":",nc)
    --small colors (big colors are saved by cd)
    for _,t in pairs(tuples) do
      color[t] = invColors[color[t]]
    end
  end
  info("Stable after "..(r-1).." round(s).")
  info("=============================")
  return color,nc,r,cd,rdist
end
local refmain = M.refmain

M.onecolor = function(a,b)
  return 0
end
local onecolor = M.onecolor


M.graphTo2dimRef = function(method,G,pcolor,inicolor,nextcolor)
  pcolor = pcolor or onecolor
  if method == "tuplesAsIntegers" then
    error("tuplesAsIntegers not implemented yet for gen. ref")
    local vertices, intToVert,tuples,color = {},{},{},{}
    local n = (type(G.V) == "number" and G.V) or #G.V
    local i=0
    for v in G:vertices() do
      vertices[i]=i
      intToVert[i]=v
      i = i+1
    end
    i=0
    for u=0,n-1 do
      for v=0,n-1 do
        tuples[i] = i
        color[i] = tostring(inicolor(G,u,v,pcolor))
        i=i+1
      end
    end
    return vertices,tuples,replaceInt(n),color,intToVert
  elseif method == "plainTuples" then
    local vertices,tuples,color = {},{},{}
    setmetatable(color,indexByTuple)
    local i=0
    for v in G:vertices() do
      vertices[i]=v
      i = i+1
    end
    i=0
    for u in G:vertices() do
      for v in G:vertices() do
        tuples[i] = {u,v}
        color[{u,v}] = tostring(inicolor(G,u,v,pcolor))
        i=i+1
      end
    end
    local refcolor = function(t,vertices,tuples,color,k)
      return nextcolor(G,t,tuples,color,k)
    end
    return vertices,tuples,refcolor,color
  end
end
local graphTo2dimRef = M.graphTo2dimRef

M.g1d2Ref = function(G,pcolor,inicolor,nextcolor,maxr)
  local v,t,rc,c = graphTo2dimRef("plainTuples",G,pcolor,inicolor,nextcolor)
  return refmain(v,t,rc,c,2,maxr)
end


M.inicolorTriv = function(G,u,v,pcolor)
  return pcolor(u,v)
end
local inicolorTriv = M.inicolorTriv

M.nextColorIncident = function(G,t,tuples,color,k)
  if t[1] ~= t[2] then
    return tostring(color[t])
  else
    local u = t[1]
    local incList = {}
    for v in G:vertices() do
      table.insert(incList,color[{u,v}].."|"..color[{v,v}])
    end
    table.sort(incList)
    return "_"..color[t] .. "(".. table.concat(incList,",")..")"
  end
end

-- local G = graphs.Graph({1,2,3,4},{{1,2},{2,3},{3,4}})
-- local pcolor = function(u,v)
--   return ((u < v-1) and 1) or 0
-- end

-- local test = {M.g1d2Ref(G,pcolor,inicolorTriv,M.nextColorIncident)}
-- print(test[4])

return M






