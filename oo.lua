--(C) 2017-2023 Frank Fuhlbrück
--This file (oo.lua) can be distrubuted and modified without any
--restrictions. If this file is distributed with other files
--this only holds for oo.lua, other parts may have more restrictive
--licences.

--- general purpose functions for object-oriented programming and more
--- Version: 20230621
--- @defmodule oo

local M = {
  MODINFO = {
    name = "oo",
    version = "20230621",
    links = {}
  }
}

--- div function for lua version prior to 5.3.
--- @return number a div b
M.div = function(a,b)
  return (a-(a%b))/b
end
if _VERSION ~= "Lua 5.2" then
  M.div = load("return function(a,b) return a//b end")()
end

--- Core of the class system from oo.lua.
--- A class has is a table equipped for usage as a metatable.
--- __index is set to the class itself or to a function
--- to look for entries in ancestor classes.
--- The class is callable, where MyClass(...) invokes
--- MyClass.new(...).
--- EXAMPLE: `MyClass = {new = ..., __tostring = ...}`
--- afterwards: `makeclass(MyClass)`
M.makeclass = function(c,inh)
    local idx = nil
    if inh then
        for _,pc in ipairs(inh) do
            for k,v in pairs(pc) do
                if not c[k] and k ~= "__index" then
                    rawset(c,k,v)
                end
            end
        end
        idx = function(o,i)
            local r = rawget(c,i)
            if r ~= nil then
                return r
            end
            for _,pc in ipairs(inh) do
                r = pc[i]
                if r then
                    return r
                end
            end
        end
        rawset(c,"__index",idx)
    end
    setmetatable(c, {
        __call = function (cls, ...)
            return cls.new(...)
        end,
        __index = idx
    })
    if c.__index == nil then
        if c.__index_chain then
            c.__index = function(o,i)
                local r = c.__index_chain(o,i)
                if not r then
                    return c[i]
                end
            end
        else
            c.__index = c
        end
    end

    if c.__concat == nil then
        c.__concat = M.concatWithToString
    end
end

--- Standard __conact method for classes: obj .. "" = tostring(obj)
--- @param t1 any
--- @param t2 any
--- @return string
M.concatWithToString = function(t1,t2)
    return  tostring(t1) ..  tostring(t2)
end

--- copying the metatable in one or two steps
--- returns target thus "return cpmt(f,t)"  is possible
--- @param f any from
--- @param t any to
--- @return any t with metatable of f
M.cpmetatable = function(f,t)
    t = t or {}
    setmetatable(t,getmetatable(f))
    return t
end
--- shortcut for cpmetatable
--- @function cpmt
M.cpmt = M.cpmetatable
local cpmt = M.cpmetatable

--- returns a function that sets the metatable of its argument to
--- the current metatable of f
M.savemt = function(f)
    local mt = getmetatable(f)
    return function(t)
        setmetatable(t,mt)
        return t
    end
end

local dcopy
--- copy tables until depth l (only copies values, not keys!).
--- @param t table
--- @param l integer depth
--- @return table 'copy of t'
M.dcopy = function(t,l)
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local ret = {}
        cpmt(t,ret)
        for k,v in pairs(t) do
            ret[k] =  dcopy(v,l-1)
        end
        return ret
    else
        return t
    end
end
dcopy = M.dcopy

--- mmipairs shall respect metamethods for all LUA versions,
--- while rawipairs shall ignore them
--- @param tab table argument for ipairs
--- @function rawipairs(tab : table)
M.rawipairs=ipairs
--- mmipairs shall respect metamethods for all LUA versions,
--- while rawipairs shall ignore them
--- @param tab table argument for ipairs
--- @function mmipairs(tab : table)
M.mmipairs=ipairs
if(_VERSION=="Lua 5.2") then
    local ipit = function(t,i)
        i = i+1
        local v = t[i]
        if v then
            return i,v
        end
    end
    M.mmipairs = function (t)
        return ipit,t,0
    end
else
    local ipit = function(t,i)
        i = i+1
        local v = rawget(t,i)
        if v then
            return i,v
        end
    end
    M.rawipairs = function (t)
        return ipit,t,0
    end
end

M.ipairsuntil = function(t,ut)
    local ipit = M.mmipairs(t)
    return function(t,i)
        local j,v = ipit(t,i)
        if v and v ~= ut then
            return j,v
        end
    end,t,0
end

local dump
--- printable table representation until depth l
M.dump = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s,lastk,lastv = pre,nil,nil
        for k,v in pairs(t) do
            if lastv ~= nil then
                s = s .. dump(lastk,l-1)..":".. dump(lastv,l-1) .. sep
            end
            lastv,lastk = v,k
        end
        return s .. dump(lastk,l-1)..":".. dump(lastv,l-1) .. post
    else
        return tostring(t)
    end
end
dump = M.dump

local dumpNoKeys
--- printable table representation until depth l (only values)
M.dumpNoKeys = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s = pre
        local lastv
        for k,v in pairs(t) do
            if lastv ~= nil then
                s = s  .. dumpNoKeys(lastv,l-1) .. sep
            end
            lastv = v
        end
        return s .. dumpNoKeys(lastv,l-1) .. post
    else
        return tostring(t)
    end
end
dumpNoKeys = M.dumpNoKeys

local idump
--- printable table repr. until depth l (only values of integer keys)
M.idump = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s = pre
        for k=1,#t do
             local v=t[k]
             s = s  .. idump(v,l-1) .. (k<#t and sep or "")
        end
        return s .. post
    else
        return tostring(t)
    end
end
idump = M.idump

--- create statefull iterator from stateless one
M.statefull = function(sl,t)
    local it, s, k = sl(t)
    return function()
        local vals = {it(s,k)}
        k = vals[1]
        return table.unpack(vals)
    end
end

--- returns table filled with entries v from for v=f,t,by do ...
M.seq = function(f,t,by)
    by = by or 1
    local ret = {}
    local i = 0
    for v=f,t,by do
        i=i+1
        ret[i] = v
    end
    return ret
end

--- checks if an iterator delivers a number n of values
--- where n is in [rmin,rmax]
M.rangemany = function(it,rmin,rmax)
    local i = 0
    while it() and i <= rmax+1 do
        i=i+1
    end
    return i >= rmin and i <= rmax
end

--- returns a subtable of a given table with indices from f to l
M.tablerange = function(t,f,l)
    local ret = {}
    for i=f,l do
        ret[#ret+1] = t[i]
    end
    return ret
end

--- glues together two array-tables t1 and t2 to a single table t
--- such that #t=#t1+#t2.
M.gluetables = function(t1,t2)
    local ret = {}
    local nt1=#t1
    for i=1,#t1+#t2 do
        ret[i] = (i <= nt1 and t1[i]) or t2[i-nt1]
    end
    return ret
end

--- sets entries t[f+i-1] to r[i] for 1<=i<=#r
M.settablerange = function(t,f,r)
    f=f-1
    for i=1,#r do
        t[i+f] = r[i]
    end
end

--- rerverses the table
M.rev = function(t)
    local ret,j = {},#t
    for i=1,#t do
        ret[j] = t[i]
        j = j-1
    end
    return ret
end

--- table of keys, s.t. keys(t)[k]=k
M.keys   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[k]=k
    end
    return ret
end

--- table of keys, s.t. ikeys(t)[i]=k, i in 1,num. of keys
M.ikeys   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[#ret+1]=k
    end
    return ret
end
local ikeys = M.ikeys

--- table of keys, s.t. keys(t)[k]=k
M.kvswap   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[v]=k
    end
    return ret
end
local kvswap = M.kvswap

--- like ikeys, but the returned table is sorted accoding to f(k1,k2,t)
--- not that t is an argument for f, thus the comparision can access
--- the corresponding values, see numval for an example.
--- EXAMPLE: `idump(ikeyssort({baa=1,aab=2}))` returns
--- `{aab, baa}`
M.ikeyssort = function(t,f)
    local ret = {}
    for k,v in pairs(t) do
        ret[#ret+1]=k
    end
    local fs = (f and function(k1,k2)
        return f(k1,k2,t)
    end) or nil
    table.sort(ret,fs)
    return ret
end

--- sort keys according to their corresponding value
M.numval = function(how)
    if how == "asc" then
        return function(k1,k2,t)
            return (t[k1] < t[k2]) or
                    (t[k1] == t[k2] and k1 < k2)
        end
    else
        return function(k1,k2,t)
            return (t[k1] > t[k2]) or
                    (t[k1] == t[k2] and k1 < k2)
        end
    end
end

--- classical functional map, but f might have two arguments v,k
--- such that the key can also be considered.
M.map = function(f,t)
    local ret = {}
    for k,v in pairs(t) do
        ret[k] = f(v,k)
    end
    return ret
end

--- map function for iterator constructors
--- EXAMPLE: `vipairs = oo.mapi(function(k,v) return v,k end,ipairs)`
--- combined with `for k,v in vipairs({2,1}) do print(k) end` prints
--- 2 and then 1.
M.mapi = function(f,it)
    return function(...)
        local i,s,v = it(...)
        return function()
            local ir = {i(s,v)}
            v = ir[1]
            return f(table.unpack(ir))
        end
    end
end

--- like map, but modifies t in place
M.mapmod = function(f,t)
    for k,v in pairs(t) do
        t[k] = f(v,k)
    end
    return t
end

--- classic Reduce function. The iter argument ensures a particular
--- order, the default value is ipairs.
M.reduce = function(f,t,init,iter)
    iter = iter or ipairs
    local ret = init
    for k,v in iter(t) do
        ret = f(ret,v,k)
    end
    return ret
end
local reduce = M.reduce

--- product over all t[k] in order of iter.
M.prod = function(t,iter)
    return reduce(function(x,y) return x*y end,t,1,iter)
end

--- sum over all t[k] in order of iter.
M.sum = function(t,iter)
    return reduce(function(x,y) return x+y end,t,0,iter)
end

--- returns
M.where = function(t,what)
    local ret,i = {},0
    for k,v in pairs(t) do
	if v == what then
            i=i+1
            ret[i]=k
        end
    end
    return ret
end

M.whichmax = function(t,iter)
    return reduce(
        function(maxpair,v,k)
            return (maxpair.v < v and {v=v,k=k}) or maxpair
        end,t,{v=0,k=0},iter
    )
end

--- transpose 2-dim table
M.transp = function(t,inplace)
    local ret = {}
    for j=1,#(t[1]) do
        ret[j] = {}
        for i=1,#t do
            ret[j][i] = t[i][j]
        end
    end
    return ret
end

M.blocks  = function(t,l)
    local bl = {}
    for i=1,#t do
        if i % l == 1 or l==1 then
            bl[#bl+1] = {}
        end
        bl[#bl][((i-1) % l)+1] = t[i]
    end
    return bl
end

M.splice  = function(t,l)
    local spl = {}
    for i=1,l do
        spl[i] = {}
    end
    for i=1,#t do
        local part = spl[((i-1) % l)+1]
        part[#part+1] = t[i]
    end
    return spl
end

M.desplice  = function(spl)
    local t,l,i = {},#spl,0
    local last = ""
    while last do
        i=i+1
        t[i] = spl[((i-1) % l)+1][((i-1)-((i-1)%l))/l+1]
        last = t[i]
    end
    return t
end

M.spliceStr  = function(s,l)
    local spl = {}
    for i=1,l do
        spl[i] = ""
    end
    for i=1,s:len() do
        local part = spl[((i-1) % l)+1]
        part = part .. s:sub(i,i)
        spl[((i-1) % l)+1] = part
    end
    return spl
end

M.despliceStr  = function(spl)
    local s,l,i = "",#spl,0
    local last = "-"
    while last ~= "" do
        i=i+1
        local j = ((i-1)-((i-1)%l))/l+1
        last = spl[((i-1) % l)+1]:sub(j,j)--gibt "" zurück,für j>len
        s = s .. last
    end
    return s
end

local chooseRec
--recursive implementation of choose iterator (for reference)
M.chooseRec = function(t,n,tmax,tshift,nshift)
    tshift = tshift or 0
    nshift = nshift or 0
    tmax = tmax or #t
    local pos,nlit = tshift,nil
    if n ~= 1 then
        nlit = chooseRec(t,n-1,tmax,tshift+1,nshift+1)
        pos=pos+1
        return function()
            local set = nlit()
            if (not set) and pos < tmax-(n-1) then
                pos = pos + 1
                nlit=chooseRec(t,n-1,tmax,pos,nshift+1)
                set = nlit()
            elseif (not set) and pos >= tmax-(n-1) then
                return nil
            end
            set[nshift+1] = t[pos]
            return set
        end
    else
        return function()
            pos = pos + 1
            return ((pos <= tmax and {[nshift+1]=t[pos]}) or nil)
        end
    end
end
chooseRec = M.chooseRec

--k-element subsets of {1,...,n}
M.chooseInd = function(n,k)
    if k > n then
      return function() return nil end
    end
    if k == 0 then
      local done = false
      return function()
        local ret=(not done and {}) or nil
        done=true
        return ret
      end
    end
    local ind = {}
    ind[1] = 0--always incrementable
    for i=2,k do
      ind[i] = n--never incrementable
    end
    return function()
      if not ind then
        return nil
      end
      local r = k
      while r >= 1 and ind[r] >= n-k+r do
        r = r - 1
      end
      if r == 0 then
        ind = nil
        return nil
      end
      ind[r] = ind[r]+1
      for i = r+1,k do
        ind[i] = ind[i-1]+1
      end
      return ind
    end
end
local chooseInd = M.chooseInd

--k-element subtables of t
M.choose = function(t,k)
  local cit = chooseInd(#t,k)
  return function()
    local ind=cit()
    if ind then
      local s = {}
      for i=1,k do
        s[i] = t[ind[i]]
      end
      return s
    else
      return nil
    end
  end
end
local choose = M.choose

M.perm = function(t,npos)
    local k = ikeys(t)
    local pos = 0
    npos = npos or 1
    if #k ~= 1 then
        local nlit = nil
        return function()
            local set = nlit and nlit()
            if (not set) and pos < #k then
                pos = pos + 1
                local tc = dcopy(t)
                tc[k[pos]] = nil
                nlit=M.perm(tc,npos+1)
                set = nlit()
            elseif (not set) and pos >= #k then
                return nil
            end
            set[npos] = t[k[pos]]
            return set
        end
    else
        local once = true
        return function()
            if once then
                once = false
                return {[npos]=t[k[1]]}
            else
                return nil
            end
        end
    end
end
local perm = M.perm

M.kperm = function(t,k)
    local chooseit = choose(t,k)
    local permit = nil
    return function()
        local kpm = permit and permit()
        if not kpm then
            local pm = chooseit()
            if not pm then
                return nil
            end
            permit = perm(pm)
            kpm = permit()
        end
        return kpm
    end
end

local ktuples
--iterates over all tpl in t^k
M.ktuples = function(t,k,nshift)
    nshift = nshift or 0
    local tmax=#t
    local pos = 0
    if k ~= 1 then
        local nlit = ktuples(t,k-1,nshift+1)
        pos=pos+1
        return function()
            local tpl = nlit()
            if (not tpl) and pos < tmax then
                pos = pos + 1
                nlit=ktuples(t,k-1,nshift+1)
                tpl = nlit()
            elseif (not tpl) and pos == tmax then
                return nil
            end
            tpl[nshift+1] = t[pos]
            return tpl
        end
    else
        return function()
            pos = pos + 1
            return ((pos <= tmax and {[nshift+1]=t[pos]}) or nil)
        end
    end
end
ktuples = M.ktuples

local ktuplesIndRec
--iterates over all tpl in {1,...,indmax[1]} x ... x {1,...,indmax[k]}
--old impl. for reference, use ktuplesInd
M.ktuplesIndRec = function(indmax,k,nshift,rev)
    nshift = nshift or 0
    local imax=(type(indmax)=="table" and indmax[nshift+1]) or indmax
    k = k or #indmax
    local i = 0
    if k > 1 then
        local nlit = ktuplesIndRec(indmax,k-1,nshift+1,rev)
        i=i+1
        if rev then
            return function()
                local tpl = nlit()
                if (not tpl) and i < imax then
                    i = i + 1
                    nlit=ktuplesIndRec(indmax,k-1,nshift+1,rev)
                    tpl = nlit()
                elseif (not tpl) and i == imax then
                    return nil
                end
                tpl[#tpl+1] = i
                return tpl
            end
        end
        return function()
            local tpl = nlit()
            if (not tpl) and i < imax then
                i = i + 1
                nlit=ktuplesIndRec(indmax,k-1,nshift+1,rev)
                tpl = nlit()
            elseif (not tpl) and i == imax then
                return nil
            end
            tpl[nshift+1] = i
            return tpl
        end
    else
        if rev then
            return function()
                i = i + 1
                return ((i <= imax and {i}) or nil)
            end
        end
        return function()
            i = i + 1
            return ((i <= imax and {[nshift+1]=i}) or nil)
        end
    end
end
ktuplesIndRec = M.ktuplesIndRec

--iterates over all tpl in {1,...,indmax[1]} x ... x {1,...,indmax[k]}
--zero: over {0,...,indmax[1]-1} x ... x {0,...,indmax[k]-1} instead
--rev: iterates in lexical order of reversed tuples
--(neither produces reversed tuples nor iterates in reversed order)
--i.e.: {1,1} {2,1} {3,1} instead of {1,1} {1,2} {2,1}
--if indmax={3,2} and zero is false/nil
M.ktuplesInd = function(indmax,zero,rev,nodcopy)
    if #indmax == 0 then
        return function() end
    end
    local t = {}
    local dcopy = (nodcopy and (function(t) return t end)) or dcopy
    indmax = dcopy(indmax)
    local init = (zero and 0) or 1
    local st,ed = (rev and #indmax) or 1, (rev and 1) or #indmax
    local by=(rev and 1) or -1
    for j=st,ed+by,-by do
        t[j] = init
        indmax[j] = indmax[j]-1+init
    end
    t[ed] = init-1
    indmax[ed] = indmax[ed]-1+init
    return function()
        for j=ed,st,by do
            t[j] = t[j] + 1
            if t[j] <= indmax[j] then--no further carry
                return dcopy(t)
            end
            t[j] = 1
        end
    end
end


M.withoutVals = function(t,vals)
  local ret = {}
  for _,v in pairs(t) do
    if not vals[v] then
      table.insert(ret,v)
    end
  end
  return ret
end
local withoutVals = M.withoutVals

--- interator over all subsets of t conaining values in withv.
--- This iterator also works if some values in withv are not in t,
--- it simply prefixes all subsets of (t \ withv) with withv.
--- @param t table (assumed to be an array)
--- @param withv values to be contained in all subsets
--- @return function iterator
M.subsetsWith = function(t,withv)
  local twov = withoutVals(t,kvswap(withv))
  local n = #twov
  assert(n <= 63, "too many subsets")
  local max = 2^n-1
  local cur = -1
  return function()
    cur = cur + 1
    if cur > max then
      return nil
    end
    local ret = dcopy(withv)
    local id = #ret
    local m=1
    for i=1,n do
      if cur & m ~= 0 then
        table.insert(ret,twov[i])
      end
      m = 2*m
    end

    return ret
  end
end
local subsetsWith = M.subsetsWith

local partitions
--- interator over all partitions of t.
--- @param t table (assumed to be an array)
--- @param startidx use indices startidx,startidx+1,... instead of 1,2,3
--- This parameter is mostly used for recursion.
--- @return function iterator
M.partitions = function(t,startidx)
  startidx = startidx or 1
  local t1 = t[1]
  local subWith1 = subsetsWith(t,{t1})
  local s1 = nil
  local subPart = nil
  return function()
    local sp = nil
    if subPart then
      sp = subPart()
    end
    if not subPart or not sp then
      s1 = subWith1()
      if not s1 then
        return nil
      end
      local trest = withoutVals(t,kvswap(s1))
      if #trest == 0 then
        return {[startidx]=s1}
      end
      subPart = partitions(trest,startidx+1)
      sp = subPart()
    end
    --now s1 and sp are both not nil -> combine
    --sp uses indices from startidx+1
    sp[startidx] = s1
    return sp
  end
end
partitions = M.partitions

M.fillWith = function(s,w,n)
    local t = {s}
    for i=1,n-#s do
        t[#t+1] = w
    end
    return table.concat(t)
end

M.applyPerm = function(t,pi)
    local tp = {}
    for i=1,#t do
      tp[i] = t[pi[i]]
    end
    return tp
end


M.isint = function(n)
  return type(n) == "number" and n == math.floor(n)
end
local isint = M.isint

--table.maxn is removed but still useful for "sparse" arrays
if not table.maxn then
---@diagnostic disable-next-line: duplicate-set-field
    table.maxn = function(t)
        local max=0
        for i,v in pairs(t) do
            if isint(i) and i > max then
                max = i
            end
        end
        return max
    end
end

--fills all values from 1 to table.maxn(t) with values according to
--t[table.maxn(t)]
M.desparsify = function(t)
    local n = table.maxn(t)
    if (n<1) then
        return
    end

    local fill = (type(t[n]) == "string" and "") or
            (type(t[n])=="number" and 0) or false
    for i=1,n do
        t[i] = t[i] or fill
    end
end

M.nilifeq = function(v,forbidden)
    return (v~=forbidden and v) or nil
end

--- linkmodules(m1,m2,...) gives each module access to functionality
--- of the other modules without creating require-loops.
--- If module m1 wants to
--- access m2.someFunction and m2.MODINFO.name = "secondMod", then
--- m1.MODINFO.links["secondMod"] shall be a function(m) that
--- assigns m.someFunction to a local variable in m1.
M.linkmodules = function(...)
  local mods = {...}
  local byname = {}
  for _,m in pairs(mods) do
    if m.MODINFO then
      byname[m.MODINFO.name] = m
    end
  end
  for _,m in pairs(mods) do
    for l,f in pairs(m.MODINFO.links or {}) do
      assert(byname[l],"check MODINFO for module "..l)
      f(byname[l])
    end
  end
end

--- returns n placeholder functions to be linked later.
--- use it as
--- `func1,func2,func3 = oo.expectlink("secondMod","firstMod",3)`
--- If any of this functions are called before linking with
--- oo.linkmodules, then an error occurs:
--- `"use linkmodules() with firstMod and secondMod module."`
M.expectlink = function(to,from,n)
  local ret = {}
  local f = function(...)
    error("use linkmodules() with ".. from .." and ".. to .." module.")
  end
  for i=1,n do
    ret[i] = f
  end
  return table.unpack(ret)
end

--- inserts every element MODINFO of M into the global namespace.
M.globalize = function(module)
  for k,v in pairs(module) do
    assert(not _G[k],"globalize: \""..k.."\" is already a global key.")
    if k ~= "MODINFO" then
      _G[k] = v
    end
  end
end

--- If set to true before `require "oo"`, all oo functions etc will
--- be part of the global namespace.
--- @field OO_USE_GLOBALS
if _G["OO_USE_GLOBALS"] then
  M.globalize(M)
end

return M
