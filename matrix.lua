--- matrices (as known from linear algebra)
--- @defmodule matrix
local oo = require "oo"
local div,seq,map,mapmod,makeclass = oo.div,oo.seq,oo.map,oo.mapmod,oo.makeclass

local M = {
  MODINFO = {
    name = "matrix",
    links = {}
  }
}

--finite checks, whether a variable is neither
--+inf,-inf noch NaN ist
local finite = function(i)
  if type(i) == 'table' and i.finite then
    return i:finite()
  end
  return (i == i and i ~= (1/0) and i ~= -(1/0))
end

local invtbl = function(i)
  if type(i) == 'table' and i.invtbl then
    return i:invtbl()
  end
  return ((i == i and i ~= (1/0) and i ~= -(1/0)) and i ~= 0)
end

local close = function(i,j)
  if type(i) == 'number' then
    return math.abs(i-j) < 10^(-13)
  end
  return i == j
end

--least significant bit and index from 0 bis r
local binRepr = function(x)
    local e,r = {},-1
    repeat
        r=r+1
        e[r] = (x % 2 == 1) and 1 or 0
        x = div((x - e[r]),2)
    until x == 0
    return e,r
end

local charPol = {
  __tostring = function(cp)
    local n = #cp
    local ret = {}
    ret[1] = "x^"..n
    for i=n-1,1,-1 do
      ret[n-i+1] = cp[i]..("x^"..i)
    end
    ret[n+1] = cp[0]
    return table.concat(ret," + ")
  end
}

local Matrix
--- Matrix entries could be elements of some ring
--- (copied from cryptology courses). This functionality is
--- currently not implemented here (exept for rationals).
--- @class Matrix
M.Matrix = {
  --- constructor.
  --- @param init number or 2dim-table or 1dim-table or Matrix
  --- @param ncol number #of columns if not implicitly given by
  --- 2dim-table or matrix as first argument.
  --- @param ring table class to which each element belongs
  --- (e.g. Rationals ;nil -> float)
  new = function(init,ncol,ring)
    local mat
    if type(init) == 'number' then
      ncol = ncol or init
      mat = Matrix.newNoInit(init,ncol,ring)
      for i=1,mat.nrow do
        mat[i] = {}
        for j=1,mat.ncol do
          mat[i][j] = 0
        end
      end
    elseif type(init) == 'table' and --1-dim-Array
        (type(init[1]) == 'number' or
         type(init[1]) == 'string' or init[1].val) then
      assert(#init % ncol == 0, "error during initialization: table length no multiple of "..ncol)
      mat = Matrix.newNoInit(div(#(init),ncol),ncol,
        ring or (type(init[1]) == 'table' and init[1].ring)
        or nil)
      for i=1,mat.nrow do
        mat[i] = {}
        for j=1,mat.ncol do
          mat[i][j] = init[(i-1)*mat.ncol+j] or 0
        end
      end
    else --2-dim-Array (nrow,ncol implizit)
      assert(type(init[1]) == 'table')
      mat = Matrix.newNoInit(#(init),ncol or #(init[1]),
        ring or (type(init[1][1]) == 'table' and init[1][1].ring)
        or nil)
      for i=1,mat.nrow do
        mat[i] = {}
        for j=1,mat.ncol do
          mat[i][j] = init[i][j] or
              ((mat.ring and mat.ring:el(0)) or 0)
        end
      end
    end
    if type(ring) == "number" then
      --TODO: not implemented yet: ring = RkRing(ring)
    end
    if ring then
      for i=1,mat.nrow do
        for j=1,mat.ncol do
          mat[i][j] = ring:el(mat[i][j])
        end
      end
    end
    if not ring and mat.nrow >= 1 and type(mat[1][1]) == 'table' then
      mat.ring = mat[1][1].ring
    end
    return mat
  end,

  --- creates a new matrix without actually creating
  --- the nested tables. Yout have to do this yourself.
  newNoInit = function(nrow,ncol,ring)
    local mat = {nrow=nrow,ncol=ncol,ring=ring}
    setmetatable(mat,Matrix)
    return mat
  end,

  --- identity matrix
  one = function(n,ring)
    local mat = Matrix(n,n,ring)
    if type(ring) == "number" then
      --TODO: not implemented here-> copy from crypto course if needed
    end
    for i=1,n do
      mat[i][i] = (ring and ring:el(1)) or 1
    end
    return mat
  end,

  --- transposed matrix.
  t = function(mat)
    local ret = Matrix.newNoInit(mat.ncol,mat.nrow,mat.ring)
    for j=1,mat.ncol do
        ret[j] = {}
        for i=1,mat.nrow do
            ret[j][i] = mat[i][j]
        end
    end

    return ret
  end,

  --- "glue" two matrices together (Standard: horizontally = h).
  glue = function(mat,mat2,dir)
    dir = dir or "h"
    if dir == "h" then
      mat = Matrix(mat)--copy
      --mat2 does not need to be copied
      if getmetatable(mat2) ~= Matrix or
          mat2.ring ~= mat.ring then
        local ncol2 = mat2.ncol or div(#mat2,mat.nrow)
        mat2 = Matrix(mat2,ncol2,mat.ring)
      end
      for i=1,mat.nrow do
        for j=1,mat2.ncol do
          mat[i][mat.ncol+j] = mat2[i][j]
        end
      end
      mat.ncol = mat.ncol + mat2.ncol
    else
      --copy both matrices and transform mat2 to a matrix if needed
      mat,mat2 = Matrix(mat),Matrix(mat2,mat.ncol,mat.ring)
      for i=1,mat2.nrow do
        mat[i+mat.nrow] = mat2[i]
      end
      mat.nrow = mat.nrow + mat2.nrow
    end
    return mat
  end,

  --sub matrix with given set of rows and columns
  sub = function(mat,rows,cols)
    rows = rows or seq(1,mat.nrow)
    cols = cols or seq(1,mat.ncol)
    local ret = Matrix(#rows,#cols,mat.ring)
    for i=1,ret.nrow do
      for j=1,ret.ncol do
        ret[i][j] = mat[rows[i]][cols[j]]
      end
    end
    return ret
  end,

  --returns a copy where all entries that are in a given row
  --AND a given column are replaced by zero
  zeroFilled = function(mat,rows,cols)
    rows = rows or seq(1,mat.nrow)
    cols = cols or seq(1,mat.ncol)
    local ret = Matrix(mat)
    for _,i in pairs(rows) do
      for _,j in pairs(cols) do
        ret[i][j] = 0
      end
    end
    return ret
  end,

  --returns a copy where all entries that are NOT in a given row
  --AND a given column are replaced by zero
  zeroFilledExcept = function(mat,rows,cols)
    rows = rows or seq(1,mat.nrow)
    cols = cols or seq(1,mat.ncol)
    local ret = Matrix(mat.nrow,mat.ncol,mat.ring)
    for _,i in pairs(rows) do
      for _,j in pairs(cols) do
        ret[i][j] = mat[i][j]
      end
    end
    return ret
  end,

  --right side (i.e. ncol rightmost columns) of a matrix
  rs = function(mat,ncol)
    ncol = ncol or div(mat.ncol,2)
    local ret = Matrix(mat.nrow,ncol,mat.ring)
    local o = mat.ncol - ret.ncol
    for i=1,ret.nrow do
      for j=1,ret.ncol do
        ret[i][j] = mat[i][o+j]
      end
    end
    return ret
  end,

  diag = function(mat)
    local dmin = (mat.nrow < mat.ncol) and mat.nrow or mat.ncol
    local ret = {}
    for i=1,dmin do
      ret[i]=mat[i][i]
    end
    return ret
  end,

  trace = function(mat)
    local dmin = (mat.nrow < mat.ncol) and mat.nrow or mat.ncol
    local ret = 0
    for i=1,dmin do
      ret = ret + mat[i][i]
    end
    return ret
  end,

  __tostring = function(mat,colwidth)
    local ts
    mat.sTrenner = mat.sTrenner or {}
    mat.zTrenner = mat.zTrenner or {}
    if mat.ring  then
      local tsd = mat.ring.elToShortString or tostring
      ts = function(el,i)
        local ret=tsd(el)
        while colwidth and #ret < colwidth do
          ret = " " .. ret
        end
        if colwidth  and mat.sTrenner[i] then
          ret = ret .. mat.sTrenner[i]
        end
        return ret
      end
    else
      ts = function(n,i)
        local ret
        if type(n) == "table" then
          ret = tostring(n)
        else
          ret = tostring(math.tointeger(n) or
            (n < 2^63 and string.format("%.3f",n)) or n)
        end
        while colwidth and #ret < colwidth do
          ret = " " .. ret
        end
        if colwidth and mat.sTrenner[i] then
          ret = ret .. mat.sTrenner[i]
        end
        return ret
      end
    end
    if not colwidth then
      colwidth = 0
      for i=1,mat.nrow do
        for j=1,mat.ncol do
          colwidth = math.max(#ts(mat[i][j]),colwidth)
        end
      end
    end
    local s = ""
    for i=1,mat.nrow do
      s = s .. table.concat(map(ts,mat[i]), "  ")
      if mat.zTrenner[i] then
        s = s .. "\n"
        local tr = ""
        while #tr < (colwidth+2)*mat.ncol-2 do
          tr = tr .. mat.zTrenner[i]
        end
        s = s .. tr
      end
      if i < mat.nrow then
        s = s .. "\n"
      end
    end
    return s
  end,

  __index = function (mat, key)
    --Für normale Schlüssel (keine Tabellen)
    --dasselbe Verhalten wie sonst durch makeclass erzeugt,
    --d.h. nicht vorhandene Einträge werden in der Klasse gesucht.
    if type(key) ~= 'table' then
      return Matrix[key]
    --falls der Schlüssel eine Zeilennummer ist, gebe Zeilenvektor zurück
    elseif #(key) == 1 and type(key[1]) == 'number'
        and key[1] > 0 then
      local ret = Matrix.newNoInit(1,mat.ncol,mat.ring)
      ret[1] = mat[key[1]]
      return ret
    end
    error("matrix index error")
  end,

  __add = function(mat,mat2)
    assert(mat.nrow == mat2.nrow and mat.ncol == mat2.ncol,
      "Matrix: dimensions do not fit")
    local ring = mat.ring or mat2.ring
    local ret = Matrix(mat.nrow,mat.ncol,ring)
    for i=1,mat.nrow do
      for j=1,mat.ncol do
        ret[i][j] = mat[i][j] + mat2[i][j]
      end
    end
    return ret
  end,

  __sub = function(mat,mat2)
    assert(mat.nrow == mat2.nrow and mat.ncol == mat2.ncol,
      "Matrix: dimensions do not fit")
    local ring = mat.ring or mat2.ring
    local ret = Matrix(mat.nrow,mat.ncol,ring)
    for i=1,mat.nrow do
      for j=1,mat.ncol do
        ret[i][j] = mat[i][j] - mat2[i][j]
      end
    end
    return ret
  end,

  __mul = function(mat,mat2)
    if type(mat) == 'number' then
      mat,mat2=mat2,mat
    end
    --scalar multiple
    if type(mat2) == 'number' then
      local ret = Matrix.newNoInit(mat.nrow,mat.ncol,mat.ring)
      for i=1,mat.nrow do
        ret[i] = map(function(x) return x*mat2 end, mat[i])
      end
      return ret
    end

    assert(mat2.nrow == mat.ncol,"dimensions do not fit")
    local ring = mat.ring or mat2.ring
    local ret = Matrix(mat.nrow,mat2.ncol,ring)
    for i=1,ret.nrow do
      for j=1,ret.ncol do
        local sum = 0
        for k=1,mat.ncol do
          sum = sum + mat[i][k] * mat2[k][j]
        end
        ret[i][j] = sum
      end
    end
    return ret
  end,

  __pow = function(mat,a)
    if a == 0 then
      return Matrix.one(mat.nrow,mat.ring)
    elseif a < 0 then
      local inv = mat:inv()
      if inv then
        return inv^(-a)
      else
        return nil
      end
    end
    --Horner scheme:
    local m = mat
    local e,r = binRepr(a)
    for i=r-1,0,-1 do
        m = m*m
        if e[i] == 1 then
            m = m*mat
        end
    end
    return m
  end,

  __eq = function(m1,m2)
    if m1.ring ~= m2.ring or m1.nrow ~= m2.nrow or
        m1.ncol ~= m2.ncol then
      return false
    end
    for i=1,m1.nrow do
      for j=1,m2.nrow do
        if not close (m1[i][j],m2[i][j]) then
          return false
        end
      end
    end
    --no (large) difference found, i.e., equal
    return true
  end,

  rowMult = function(mat,i,c)
    mapmod(function(x) return c*x end, mat[i])
    --correct determinant if previously calculated
    if mat.detpre then
      mat.detpre = mat.detpre * c
    end
  end,

  colMult = function(mat,j,c)
    for i=1,mat.nrow do
      mat[i][j] = c*mat[i][j]
    end
    --correct determinant if previously calculated
    if mat.detpre then
      mat.detpre = mat.detpre * c
    end
  end,

  --mat[i] becomes mat[i] + c*mat[i2]
  rowAdd = function(mat,i,c,i2)
    if i == i2 then
      return mat:rowMult(i,c+1)
    end
    mapmod(function(x,j) return x+c*mat[i2][j] end, mat[i])
    --determinant does not change
  end,

  colAdd = function(mat,j,c,j2)
    if j == j2 then
      return mat:colMult(j,c+1)
    end
    for i=1,mat.nrow do
      mat[i][j] = mat[i][j] + c*mat[i][j2]
    end
  end,

  rowSwap = function(mat,i,j)
    mat[i],mat[j] = mat[j],mat[i]
    --correct determinant if previously calculated
    if mat.detpre then
      mat.detpre = -mat.detpre
    end
  end,

  colSwap = function(mat,i,j)
    for k=1,mat.nrow do
      mat[k][i],mat[k][j] = mat[k][j],mat[k][i]
    end
    --correct determinant if previously calculated
    if mat.detpre then
      mat.detpre = -mat.detpre
    end
  end,

  --for fields (e.g., reals)
  gaussSimple = function(mat,maxrounds,matpre,startround)
    maxrounds = maxrounds or mat.nrow
    --number of invertible elements on diagonal so far
    local ninvtbl = 0
    mat = matpre or Matrix(mat)
    startround = startround or 1
    local cprod = 1
    for round=startround,math.min(mat.nrow,mat.ncol,maxrounds) do
      local iinvtbl, c = nil, nil
      for i=round,mat.nrow do
        c = mat[i][round]
        if invtbl(c) then
          iinvtbl = i
          break
        end
      end

--       print(round)
--       print(mat)
      if not iinvtbl then
        cprod = 0--det is 0
        local jinvtbl = nil
        --maxrounds might be smaller than mat.nrow
        --we do not use mat.ncol as we
        --usually have two matrices glued together
        --we start from round+1 as mat[i][round]
        --is not invertible

        for i=round,mat.nrow do
          for j=round+1,maxrounds do
            c = mat[i][j]
            if invtbl(c) then
              jinvtbl = j
              iinvtbl = i
              break
            end
          end
          if jinvtbl then
            break
          end
        end
        if jinvtbl then
          mat:colSwap(jinvtbl,round)
        end
      end

      --still no invertible element? -> only 0 remains
      if not iinvtbl then
        break
      end

      ninvtbl = ninvtbl + 1
      cprod = cprod * c
      c = c^(-1)
      if iinvtbl ~= round then
        mat:rowSwap(iinvtbl,round)
        cprod = -cprod
      end
      mat:rowMult(round,c)

      for i=1,mat.nrow do
        if i ~= round then
          local c = -mat[i][round]
          mat:rowAdd(i,c,round)
        end
      end
    end
--     if cprod == 0 then
--       mat = nil
--     end
    return mat, cprod, ninvtbl
  end,

  --for fields (e.g., reals)
  --assume that that there is some
  --r such that the first r columns are linearly independent
  --and every other columns is a linear combination of
  --these r columns
  --return those linear comb.
  linComb = function(mat)
    local lc = {}
    mat = Matrix(mat)
    local r = 0
    for round=1,mat.ncol do
      for i=round,mat.nrow do
        if invtbl(mat[i][round]) then
          if i==round then
            break
          end
          mat:rowSwap(i,round)
          break
        end
      end

      local c = (mat[round] or {})[round]

      if not c or not invtbl(c) then
        break
      end

      r = r+1

      c=c^(-1)
      mat:colMult(round,c)
      lc[round] = lc[round] or {}
      lc[round][round] = c

      for j=round,mat.ncol do
        lc[j] = lc[j] or {}
        if j ~= round then
          local cj = -mat[round][j]
          lc[j][round] = -c*cj
          for i=1,round-1 do
            lc[j][i] = lc[j][i] + c*cj*lc[round][i]
          end
          mat:colAdd(j,cj,round)
        end
      end
    end

    for j=1,r do
      lc[j] = nil
    end

    return r,lc
  end,

  inv = function(mat)
--     if mat.nrow ~= mat.ncol then
--       return nil
--     end
    --use previously calculated value if it exists
    if mat.invpre then
      return mat.invpre
    end
    if not mat.ring then --Matrix aus float
      return mat:invFloat()
    end
    --rings currently not implemented here
  end,

  invFloat = function(mat)
    local ext = mat:glue(Matrix.one(mat.nrow,mat.ring),"h","  |")
    local cprod = nil
    ext,cprod,mat.rankpre = ext:gaussSimple()
    if cprod == 0 then
      mat.detpre = 0
      return nil
    end
    mat.invpre = ext:rs()
    mat.detpre = cprod
    return mat.invpre
  end,

  det = function(mat)
    --try to invert the matrix, doing this always
    --(even if it fails) computes the determinant
    mat:inv()
    return mat.detpre
  end,

  rank = function(mat)
    --try to invert the matrix, doing this always
    --(even if it fails) computes the rank
    mat:inv()
    return mat.rankpre
  end,

  filteredPowerMatrix = function(mat,filter,sym,ncol)
    local n = mat.nrow
    ncol = ncol or n
    if not filter then
      filter = {}
      for i=1,n do
        filter[i] = 1
      end
    end
    local tmat = (sym and mat) or mat:t()
    filter = Matrix(filter,n,mat.ring)--row vector
    local fpm = Matrix.newNoInit(ncol,n,mat.ring)
    fpm[1] = filter[1]
    local fpmcol  = filter
    for i=1,ncol-1 do
      fpmcol = fpmcol * tmat
      fpm[i+1] = fpmcol[1]
    end
    return fpm:t()
  end,

  --filter is currently unused
  powerMatrixDiagonals = function(mat,filter,ncol)
    local n = mat.nrow
    ncol = ncol or n
    local pmat = Matrix(mat)
    local pmd = Matrix.newNoInit(n,ncol)
    for i=1,n do
      pmd[i] = {1,0}
    end
    for j=2,ncol do
      pmat = pmat * mat
      for i=1,n do
        pmd[i][j] = pmat[i][i]
      end
    end
    return pmd
  end,

  --via Faddeev–LeVerrier algor.
  charPol = function(mat)
    local n = mat.nrow
    if n ~= mat.ncol then
      return nil
    end

    local c = {[n]=1}
    local matk = Matrix(n)
    local mat_matk = matk
    local id = Matrix.one(n)

    local c_n_k = 1
    for k=1,n do
      matk = mat_matk+c_n_k*id
      mat_matk = mat*matk
      c_n_k = -(1/k)*mat_matk:trace()
      c[n-k] = c_n_k
    end

    return setmetatable(c,charPol)
  end,
}
makeclass(M.Matrix)
Matrix=M.Matrix

return M
