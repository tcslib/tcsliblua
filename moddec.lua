--- modular decompositions
--- @defmodule moddec

local oo = require "oo"
local dumpNoKeys = oo.dumpNoKeys

local M = {
  MODINFO = {
    name = "moddec",
    links = {}
  }
}


local isSplitter = function(G,M,x)
  for _,u in pairs(M) do
    if u == x then
      return false
    end
  end
  for _,u in pairs(M) do
    for _,v in pairs(M) do
      if u~= v and (G:hasEdge(v,x) ~= G:hasEdge(u,x)) then
        return true
      end
    end
  end
  return false
end


--- assume G is a connected and co-connected graph
--- then u and v are in the same quasi-max. module iff
--- the closure under split relation is **not** G.V.
--- @param G Graph
--- @param u any
--- @param v any
local sameQMaxMod = function(G,u,v)
  local M = {u,v}
  local spfound = true

  while spfound do
    spfound = false
    for x in G:vertices() do
      if x~=u and x~= v  then
        if isSplitter(G,M,x) then
          table.insert(M,x)
          spfound = true
          break
        end
      end
    end
  end

  return #M < G:n()
end

--- computes quasimax. modules in a naive way via splitters;
--- used for reference to obtain test results.
--- @param G Graph
--- @return any[][] mod. dec.
--- @return table<any,any[]> mods indexed by vertices
local modDecNaive = function(G)
  local cc,ccv = G:conComp()
  if #cc > 1 then return cc,ccv end
  cc,ccv = G:complement():conComp()
  if #cc > 1 then return cc,ccv end

  local md, modbyv = {}, {}
  for v in G:vertices() do
    for _,M in pairs(md) do
      if sameQMaxMod(G,M[1],v) then
        table.insert(M,v)
        modbyv[v] = M
        break
      end
    end
    if not modbyv[v] then
      local M = {v}
      modbyv[v] = M
      table.insert(md,M)
    end
  end
  return md, modbyv
end


local refineForest = function(forest)
  return forest
end

local modDecTree
modDecTree = function(G)
  local pivot = G:vertices(1)
  local N0 = G:neighborhood(pivot)
  local T0 = modDecTree(G[N0])
end

--- computes moddec tree
--- NOT IMPLEMENTED.
--- @param G Graph
--- @return any[][] mod. dec.
--- @return Graph mod. dec. tree
M.modDecTree = function(G)
end

--- computes the modular decomposition (as a partition)
--- @param G Graph
--- @return any[][] mod. dec.
--- @return table<any,any[]> mods indexed by vertices
M.modDec = function(G)
end

M.modDec = modDecNaive

return M




