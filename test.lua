local oo = require "oo"
local dumpNoKeys,seq = oo.dumpNoKeys,oo.seq
local Matrix = (require "matrix").Matrix
local graphs = require "graphs"
local Graph,Tree,Path,Cycle = graphs.Graph,graphs.Tree,graphs.Path,graphs.Cycle
local Rook,CaleyZnZm = graphs.Rook,graphs.CaleyZnZm
local indref = require "indref"
local canonOrderIR,colorRefinement,colorPartition = indref.canonOrderIR,indref.colorRefinement,indref.colorPartition
local Rationals = (require "rationals").Rationals
local BigInt = (require "bigint").BigInt
local wl = require "wl"
wl.print = true
local g1d1WL,g2d1WL,g1d2WL,g2d2WL,g1d2WLPlain = wl.g1d1WL,wl.g2d1WL,wl.g1d2WL,wl.g2d2WL,wl.g1d2WLPlain
local moddec = require "moddec"
local modDec = moddec.modDec
oo.linkmodules(graphs,indref)


--Graphs:
print("list of two graphs:")
local list = Graph.fromAdjacencyMatrixStringMulti([[
garbage before graphs

01
10

garbage between graphs 0

 0 1 0 1 1
 1,0,0,0,0
 00011
 1-0-1-0-0
 1x0x1x0x0
]])
for _,G in pairs(list) do
  print(G)
end
print("end of list\n===========\n")

local G = Graph({1,2,3,4,5},{{1,2},{2,3},{1,3},{3,4},{3,5},{4,5}})
print(G)


local G1,G2 = G:match2Flip({2,1},{5,4})
print(G1)
print(G2)
print(G:isIsomorphicTo(G1),G:isIsomorphicTo(G2),G2:isIsomorphicTo(G1))
print(G:isTree())
print(Graph({1,2,3},{{1,2}}):isTree())
print(Graph({1,2,3,4},{{1,2},{2,3},{2,4}}):isTree())

--modular decomposition:
local G = Graph({1,2,3,4,5,6},{{1,2},{1,3},{1,4},{4,5},{2,6},{3,6}})
print(dumpNoKeys(modDec(G),2))

--Rationals:
local q = Rationals(2,3)
print(q)
print(q:inv())
print(q*q)
print(q*q-q)
print(q:inv()+q)
print(q/q)
print(q^(-2))
local m = Matrix({1,2,3,4,5,6,7,10,9},3,Rationals)
print(m)

--BigInt:
print("BigInt:")
local bi = BigInt(2344)
local fl = 2344.0
local op = function(a)
  a = a*35567*3928392839*2637263*84003963
  a = (a*a-(a*a/2)) / 100000
  return a
end
print(op(bi))
print(op(fl))

local BIRing = {el = function(_,n) return BigInt(n) end}
local m = Matrix({1,2,3,4,5,6,7,10,9},3,BIRing)
local mf = Matrix({1.0,2.0,3.0,4.0,5.0,6.0,7.0,10.0,9.0},3)
print(m^55)
print("-------")
print(mf^55)

print("\n\n")

print(126193222247400+166986974454336)
print(BigInt(126193222247400)+166986974454336)


--Matrices
local m = Matrix({1,2,3,4,5,6,7,10,9},3)
print("--Matrix op.------")
print("m:")
print(m)
print("determinant",m:det())
print("inverse:")
print(m:inv())
print("rank",m:rank())
local mi = m:inv()
print("m*m^(-1)=I?  ",m*mi == Matrix.one(3))
print("m^2:")
print(m*m)

print([[
--given a matrix with the first r columns linearly independent
compute r and the linear combination for each of the remaining
vectors:
]])
local m = Matrix({
1,-2,4,-7,4,
2,-4,6,-12,6,
3,-5,10,-18,11,
},5)
print("m:")
print(m)
local r,lc =m:linComb()
print("rank and lin. comb:")
print(r,dumpNoKeys(lc,2))

local m = Matrix({
1,-2,4,-7,4,
2,-4,6,-12,6,
3,-5,10,-18,11,
},5,Rationals)
print("m:")
print(m)
local r,lc =m:linComb()
print("rank and lin. comb:")
print(r,dumpNoKeys(lc,2))

local m = Matrix({0,1,2,0,2,4,0,3,6},3)
print("--Matrix op.------")
print("m:")
print(m)
print("determinant",m:det())
print("inverse:")
print(m:inv())
print("rank",m:rank())

local m2 = Matrix(seq(21,29),3)
print(m2)
print(m2:charPol())

print("--Walk matrix.------")
m = Matrix({0,1,1,1,0,0,1,0,0},3)
print("--Matrix op.------")
print("m:")
print("walk matrix:")
local w = m:filteredPowerMatrix()
print(w)
print("its rank:",w:rank())

print("Path of length 4")
local p = Path(5)
m=p:adjMatrix()
print(p)
print("walk matrix")
local w = m:filteredPowerMatrix()
print(w)
print("its rank:",w:rank())

print("Cycle of length 5")
local c = Cycle(5)
m=c:adjMatrix()
print(c)
print("walk matrix")
local w = m:filteredPowerMatrix()
print(w)
print("its rank:",w:rank())

print("degree sequence walks")
print(p:degereeSequenceWalks(2))
print(p:degereeSequenceWalks(3))

print("\n")
local t = Tree(2,3,3,3,1,1,3,1,1,1,1)
print(t)
print("layout:\n"..t:layout(true))
print("walk matrix")
local w = t:walkMatrix()
print(w)
print("its rank:",w:rank())
local tcanord = canonOrderIR(t)
print("orbits:")
print(dumpNoKeys(t:orbits(--[[vcolor:]]nil,tcanord),2))
print("walk matrix partition:")
print(dumpNoKeys(t:walkMatrixPartition(nil,tcanord),2))
print("path matrix")
local p = t:pathMatrix()
print(p)
print("its rank:",p:rank())
print(dumpNoKeys({t:levseq()},2))
print(dumpNoKeys({t:degseq()},2))
print(dumpNoKeys(Tree.levseqToDegseq(1,2,3,4,4,3,2,3,3,4,4)))

local t = Tree(2,3,3,1,2,1,2,1,1)
print(t:layout(true))
local tls = t:levseq()
local t2 = Tree.fromLevelSequence(tls)
print(t2:layout(true))
local col = colorRefinement(t)
print(dumpNoKeys(col))
print(dumpNoKeys((colorPartition(t,col)),2))
local tord,t2ord = canonOrderIR(t), canonOrderIR(t2)
print("canonical orders of t and t2")
print(dumpNoKeys(tord))
print(dumpNoKeys(t2ord))
print(t:adjString(tord))
print(t2:adjString(t2ord))
print(t:isIsomorphicTo(t2))
print("walk matrix for both:")
print(t:walkMatrix())
print("--")
print(t2:walkMatrix())
print("different, but are they equal after sorting?")
print(t:walkMatrixSortedString() == t2:walkMatrixSortedString())
print("sorted:")
print(t:walkMatrixSortedString())

local t3 = Tree.fromLevelSequence(1,2,2,2,2,2,2,2,2,2)
canonOrderIR(t3)


print("\n\n======k-WL ====\n")

local c3m2 = Cycle(3):disjointUnion(Cycle(3))
local c6 = Cycle(6)
print("2C3 vs. C6 1WL")
local color,nc,r,cd,rdist = g2d1WL(c3m2,c6)
print(nc,r,rdist)
print(cd)
print("2C3 vs. C6 2WL")
local color,nc,r,cd,rdist = g2d2WL(c3m2,c6)
print(nc,r,rdist)
print(cd)

local indiv1 = function(v)
  return (v==1 and 0) or 1
end

print("2C3 vs. C6 with 1 vert. indv. 1WL")
g2d1WL(c3m2,c6,indiv1,indiv1)

local r = Rook(4)
local shr = CaleyZnZm(4,4,{{0,1},{1,0},{1,1}})

--uncolored
print("2-WL on Rooks")
local twl_r = g1d2WL(r)
print("2-WL on Shrikhande")
local twl_shr = g1d2WL(shr)
print("2-WL on Rooks+Shrikhande")
local twl = g2d2WL(r,shr)


local indiv00 = function(v)
  return (v[1]==0 and v[2]==0 and 0) or 1
end

print("2-WL on Rooks with 00 indiv.")
local twl_r_indiv00 = g1d2WL(r,indiv00)
print("2-WL on Shrikhande with 00 indiv.")
local twl_shr_indiv00 = g1d2WL(shr,indiv00)
print("2-WL on Rooks+Shrikhande with each 00 indv. with same color")
local twl_indiv00 = g2d2WL(r,shr,indiv00,indiv00)
--15 colors rooks graph, 22 colors Shrikhande graph

