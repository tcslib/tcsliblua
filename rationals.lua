--- rationals (as known from school algebra)
--- @defmodule rationals
local oo = require "oo"
local makeclass = oo.makeclass

local M = {
  MODINFO = {
    name = "rationals",
    links = {}
  }
}

--forward declaration
local Rationals

--finite checks, whether a variable is neither
--+inf,-inf noch NaN ist
local finite = function(i)
  if type(i) == 'table' and i.finite then
    return i:finite()
  end
  return (i == i and i ~= (1/0) and i ~= -(1/0))
end

local close = function(i,j)
  if type(i) == 'number' then
    return math.abs(i-j) < 10^(-13)
  end
  return i == j
end

local iszero = function(x)
    if not getmetatable(x) or type(x) == "string" then
        return (x == 0)
    else
        return x:istnull()
    end
end

--recursive impl. of euclidean algor.
euklid = function(a,b)
    if iszero(b) then
        return a
    else
        return(euklid(b,a % b))
    end
end

local canForm = function(num,den)
  local gcd = euklid(num,den)
  return num//gcd,den//gcd
end

local assertQ = function(q1,q2)
  if getmetatable(q1) ~= Rationals then
    q1 = Rationals(q1)
  end
  if not q2 then
    return q1,q1
  end
  if getmetatable(q2) ~= Rationals then
    q2 = Rationals(q2)
  end
  return q1,q2
end


--- rational numbers
--- @class Rationals
M.Rationals = {
  --- constructor.
  --- @param num integer numerator
  --- @param den integer denominator
  new = function(num,den)
    den = den or 1
    assert(den > 0, "denominator must be positive")
    num = math.tointeger(num) or 1
    den = math.tointeger(den) or 1
    assert(num and den, "num and den must be integers")
    num,den = canForm(num,den)
    local q = {num=num,den=den or 1}
    setmetatable(q, Rationals)
    return q
  end,
  
  el = function(_,init)
    return Rationals(init)
  end,

  __tostring = function(q)
    return "(".. q.num .. "/" .. q.den .. ")"
  end,
  
  __unm = function(q)
    return Rationals(-q.num,q.den)
  end,

  __add = function(q,q2)
    q,q2 = assertQ(q,q2)
    local lcd = q.den*(q2.den//euklid(q.den,q2.den))
    return Rationals(q.num*(lcd//q.den)+q2.num*(lcd//q2.den),lcd)
  end,

  __sub = function(q,q2)
    return q+(-q2)
  end,

  __mul = function(q,q2)
    q,q2 = assertQ(q,q2)
    return Rationals(q.num*q2.num,q.den*q2.den)
  end,
  
  invtbl = function(q)
    return not iszero(q.num)
  end,
  
  inv = function(q)
    assert(not iszero(q.num),"(0/1) ist not invertible")
    local sign = q.num < 0 and -1 or 1
    return Rationals(sign*q.den,sign*q.num)
  end,
  
  __div = function(q,q2)
    q,q2 = assertQ(q,q2)
    return q*(q2:inv())
  end,
  
  __pow = function(q,e)
    e = math.tointeger(e)
    assert(e,"only integer exponents are allowed")
    if e < 0 then 
      q = q:inv()
      e = -e
    end
    return Rationals(q.num^e,q.den^e)
  end,
  
  __eq = function(q,q2)
    return q.num == q2.num and q.den == q2.den
  end,
}
makeclass(M.Rationals)
Rationals=M.Rationals

return M
