--- (quite naive version of) individualization-refinement
--- procedure. 
--- @defmodule indref

local oo = require "oo"
local dump = oo.dump

-- local graphs = require "graphs"

local M = {
  MODINFO = {
    name = "indref",
    links = {}
  }
}

local onecolor = function(v)
  return 0
end

--- Ensures that color is a function and not nil or a table.
--- @param color table|nil|function
--- @return function
M.colorfunc = function(color)
  color = color or onecolor
  if type(color) == "table" then
    local colt = color
    color = function(v) return colt[v] end
  end
  return color
end
local colorfunc = M.colorfunc

local combine = function(col1,col2)
  col1 = colorfunc(col1)
  col2 = colorfunc(col2)
  return function(v)
    return "[".. col1(v) ..",".. col2(v) .."]"
  end
end

M.individualize = function(color,v,tagi,tagni)
  tagi, tagni = tagi or "i:",tagni or "n:"
  color = colorfunc(color)
  return function(u)
    if u == v then
      return tagi .. color(u)
    else
      return tagni .. color(u)
    end
  end
end
local individualize = M.individualize

M.colorPartition = function(G,vcolor)
  vcolor = colorfunc(vcolor)
  local color,colors,part = {},{},{}
  for v in G:vertices() do
    local vc = tostring(vcolor(v))
    part[vc] = part[vc] or {}
    table.insert(part[vc],v)
  end
  
  local nc = #part
  local prefl = #tostring(G:n())+1
  local fmt = "%0"..(prefl-1).."d:%s"
  
  for c,p in pairs(part) do
    table.insert(colors,string.format(fmt,#p,c))
  end
  
  table.sort(colors)
  local invColors,ipart = {}, {}
  for i=1,#colors do
    ipart[i] = part[colors[i]:sub(prefl+1)]
  end
  
  return ipart,part,colors
end
local colorPartition = M.colorPartition

local colorRefinement
local canonOrderIR
M.canonOrderIR = function(G,vcolor)
  local cr,nc = colorRefinement(G,vcolor)
  vcolor = combine(vcolor,cr)
  if nc ~= G:n() then
    local ipart = colorPartition(G,vcolor)
    local i = 1
      assert(ipart[i],G:n()..":"..i.."dump"..dump(ipart))
    while #(ipart[i]) == 1 do
      i = i+1
      assert(ipart[i],G:n()..":"..i)
    end
    local maxadj,ord = "",nil
    local twCheck = {}
    --pairs would also be ok, but ipairs might be easier to debug
    for _,v in ipairs(ipart[i]) do
      local tw = false
      for _,u in pairs(twCheck) do
        tw = G:areTwins(u,v)
        if tw then break end
      end
      if not tw then
        table.insert(twCheck,v)
        local vind = individualize(vcolor,v)
        local vord = canonOrderIR(G,vind)
        local vadj = G:colorString(vcolor,vord)..G:adjString(vord)
        if vadj > maxadj then
          maxadj = vadj
          ord = vord
        end
      end
    end
    return ord
  else
    local ord = colorPartition(G,vcolor)
    for i=1,#ord do
      ord[i] = ord[i][1]
    end
    return ord
  end
end
canonOrderIR = M.canonOrderIR

M.colorRefinement = function(G,vcolor,doprint)
  vcolor = colorfunc(vcolor)
  local nc,nclast = 0,0 --current and last iteration's number of colors
  
  local color,colors = {},{}
  for v in G:vertices() do
    table.insert(colors,vcolor(v))
  end
  table.sort(colors)
  local invColors = {}
  for i=1,#colors do
    if not invColors[colors[i]] then
      invColors[colors[i]] = nc
      nc = nc + 1
    end
  end
  if doprint then print("# of colors initially:     ",nc) end
  
  for v in G:vertices() do
    color[v] = invColors[vcolor(v)]
  end
  
  local r=0
  while nc ~= nclast do
    r=r+1
    local lastcol = color
    color = {}
    local colors = {}
    for v in G:vertices() do
      color[v] = {}
      for u in G:neighbors(v) do
        table.insert(color[v],lastcol[u])
      end
      table.sort(color[v])
      color[v] =  lastcol[v] .. ',' .. table.concat(color[v],",")
      table.insert(colors,color[v])
    end
    table.sort(colors)
    nclast=nc
    nc = 0
    local invColors = {}
    for i=1,#colors do
      if not invColors[colors[i]] then
        invColors[colors[i]] = nc
        nc = nc + 1
      end
    end
    if doprint then print("# of colors after round " ..r..":",nc) end
    --small colors (TODO: save big colors)
    for v in G:vertices() do
      color[v] = invColors[color[v]]
    end
  end
  return color,nc
end
colorRefinement = M.colorRefinement

return M




